create or replace view SQL2_[FELADATSZÁM]_FZ1AU9 as;

select * from dzh.customers;

create or replace view SQL2_11_FZ1AU9 as
select city
from dzh.stores sr left outer join dzh.stocks st on sr.store_id = st.store_id
group by city
having count(*) < 2;

create or replace view SQL2_21_FZ1AU9 as
select st.store_name, st.store_id, count(o.order_id) as order_cnt
from dzh.stores st left outer join dzh.orders o on st.store_id = o.store_id
group by st.store_name, st.store_id;

create or replace view SQL2_22_FZ1AU9 as
select sub1.store_name, sub1.store_id, sub1.order_cnt, nvl(sub2.order_cnt, 0)
from
(select st.store_name, st.store_id, count(o.order_id) as order_cnt
from dzh.stores st left outer join dzh.orders o on st.store_id = o.store_id
group by st.store_name, st.store_id) sub1 left outer join 
(select st.store_id, count(product_id) as order_cnt
from dzh.stores sr inner join dzh.stocks st on sr.store_id = st.store_id
group by st.store_id) sub2 on sub1.store_id = sub2.store_id;

commit;

select store_id, count(product_id)
from dzh.stocks
group by store_id;

create or replace view SQL2_22_FZ1AU9 as
select sub1.store_name, sub1.store_id, sub1.order_cnt, nvl(sub2.order_cnt, 0) as product_cnt
from
(select st.store_name, st.store_id, count(o.order_id) as order_cnt
from dzh.stores st left outer join dzh.orders o on st.store_id = o.store_id
group by st.store_name, st.store_id) sub1 left outer join 
(select st.store_id, count(product_id) as order_cnt
from dzh.stores sr inner join dzh.stocks st on sr.store_id = st.store_id
group by st.store_id) sub2 on sub1.store_id = sub2.store_id;

create or replace view SQL2_31_FZ1AU9 as
select s.staff_id as id, s.first_name || ' ' || s.last_name as name, count(o.order_id) as num_of_orders
from dzh.staffs s left outer join dzh.orders o on s.staff_id = o.staff_id
group by s.staff_id, s.first_name, s.last_name;

select s.staff_id as id, s.first_name || ' ' || s.last_name as name, count(o.order_id) as num_of_orders
from dzh.staffs s left outer join dzh.orders o on s.staff_id = o.staff_id
group by s.staff_id, s.first_name, s.last_name;

select list_price, discount from dzh.order_items;

select sub1.id, sum(sub2.list_price * sub2.discount) as all_price
from
(select s.staff_id as id, o.order_id
from dzh.staffs s left outer join dzh.orders o on s.staff_id = o.staff_id) sub1 inner join dzh.order_items sub2 on sub1.order_id = sub2.order_id
group by sub1.id;

create or replace view SQL2_32_FZ1AU9 as
select sub3.id, sub3.name, sub3.num_of_orders, nvl(sub4.all_price, 0) as all_price
from  
(select s.staff_id as id, s.first_name || ' ' || s.last_name as name, count(o.order_id) as num_of_orders
from dzh.staffs s left outer join dzh.orders o on s.staff_id = o.staff_id
group by s.staff_id, s.first_name, s.last_name) sub3 left outer join 
(select sub1.id, sum(sub2.list_price * sub2.discount) as all_price
from
(select s.staff_id as id, o.order_id
from dzh.staffs s left outer join dzh.orders o on s.staff_id = o.staff_id) sub1 inner join dzh.order_items sub2 on sub1.order_id = sub2.order_id
group by sub1.id) sub4 on sub3.id = sub4.id;

commit;

select oi.product_id, count(o.order_id) as order_cnt
from dzh.orders o left outer join dzh.order_items oi on o.order_id = oi.order_id
group by oi.product_id;

select product_id, product_name from dzh.products;

create or replace view SQL2_41_FZ1AU9 as
select sub1.product_id, sub1.product_name, nvl(sub2.order_cnt, 0) as product_cnt
from
(select product_id, product_name from dzh.products) sub1 left outer join
(select oi.product_id, count(o.order_id) as order_cnt
from dzh.orders o left outer join dzh.order_items oi on o.order_id = oi.order_id
group by oi.product_id) sub2 on sub1.product_id = sub2.product_id;

commit;

create or replace view SQL2_51_FZ1AU9 as
select o.order_id, c.city || ',' || c.street as address  
from dzh.orders o left outer join dzh.customers c on o.customer_id = c.customer_id;

create or replace view SQL2_52_FZ1AU9 as
select o.order_id, c.customer_id, o.order_date, lag(o.order_date, 1) over(partition by c.customer_id order by o.order_date) as order_date_last 
from dzh.orders o left outer join dzh.customers c on o.customer_id = c.customer_id;


create or replace view SQL2_61_FZ1AU9 as
select customer_id, count(*) as num_of_orders 
from dzh.orders 
group by customer_id;


select customer_id, count(*) as num_of_orders 
from dzh.orders 
group by customer_id;

commit;
