-- 1. Feladat 
SELECT town_name, 
       Count(people.id) young_people 
FROM   towns 
       left outer join (SELECT * 
                        FROM   people 
                        WHERE  ( SYSDATE - date_of_birth ) / 365 < 25) people 
                    ON people.postal_code = towns.postal_code 
GROUP  BY towns.postal_code, 
          town_name; 

-- 1+ Feladat   
SELECT town_name, 
       Nvl(young_people, 0) AS young_people, 
       all_people, 
       Nvl(Round(young_people / all_people * 100, 2), 0)     AS 
       ratio_of_young_people, 
       population, 
       Nvl(Round(young_people / all_people * population), 0) AS estimated_young 
FROM   towns 
       left outer join (SELECT postal_code, 
                               Count(id) all_people 
                        FROM   people 
                        GROUP  BY postal_code) all_p 
                    ON all_p.postal_code = towns.postal_code 
       left outer join (SELECT postal_code, 
                               Count(id) young_people 
                        FROM   people 
                        WHERE  ( SYSDATE - date_of_birth ) / 365 < 25 
                        GROUP  BY postal_code) y_p 
                    ON y_p.postal_code = towns.postal_code; 

-- 2. Feladat 
SELECT town_name, 
       Nvl(SUM(pubs.capacity), 0) AS sum_of_pub_capacity, 
       Nvl(young_people, 0) 
FROM   towns 
       left outer join (SELECT postal_code, 
                               Count(id) AS young_people 
                        FROM   people 
                        WHERE  ( SYSDATE - date_of_birth ) / 365 < 25 
                        GROUP  BY postal_code) y_p 
                    ON y_p.postal_code = towns.postal_code 
       left outer join pubs 
                    ON pubs.town_postal_code = towns.postal_code 
GROUP  BY town_name, 
          young_people; 

-- 2+ Feladat   
SELECT town_name, 
       Nvl(Round(young_people / all_people * population), 0) AS estimated_young, 
       Nvl(sum_of_pub_capacity, 0)                           AS 
       sum_of_pub_capacity 
FROM   towns 
       left outer join (SELECT postal_code, 
                               Count(id) all_people 
                        FROM   people 
                        GROUP  BY postal_code) all_p 
                    ON all_p.postal_code = towns.postal_code 
       left outer join (SELECT postal_code, 
                               Count(id) young_people 
                        FROM   people 
                        WHERE  ( SYSDATE - date_of_birth ) / 365 < 25 
                        GROUP  BY postal_code) y_p 
                    ON y_p.postal_code = towns.postal_code 
       left outer join (SELECT town_postal_code, 
                               SUM(capacity) sum_of_pub_capacity 
                        FROM   pubs 
                        GROUP  BY town_postal_code) pub_c 
                    ON pub_c.town_postal_code = towns.postal_code; 

-- 3. Feladat   
SELECT Count(pub_id) AS num_of_pubs 
FROM   pubs 
WHERE  town_postal_code IN (SELECT postal_code 
                            FROM   people 
                            WHERE  first_name = 'Enikő' 
                                   AND last_name = 'Kiss'); 

-- 4. feladat   
SELECT (SELECT towns.town_name 
        FROM   people 
               inner join towns 
                       ON towns.postal_code = people.postal_code 
        WHERE  date_of_birth = (SELECT Max(date_of_birth) 
                                FROM   people)) AS legfiatalabb_varosa, 
       (SELECT towns.town_name AS legfiatalabb_varosa 
        FROM   people 
               inner join towns 
                       ON towns.postal_code = people.postal_code 
        WHERE  date_of_birth = (SELECT Min(date_of_birth) 
                                FROM   people)) AS legidosebb_varosa 
FROM   dual; 

-- 5. feladat   
SELECT town_name, 
       job, 
       Count(*) dolgozok_szama 
FROM   people 
       inner join towns 
               ON towns.postal_code = people.postal_code 
GROUP  BY job, 
          people.postal_code, 
          town_name 
ORDER  BY town_name, 
          job; 

-- 6. feladat   
SELECT town_name AS legtobb_doktor_varosa 
FROM   towns 
       inner join people 
               ON people.postal_code = towns.postal_code 
WHERE  people.job = 'doctor' 
GROUP  BY people.postal_code, 
          towns.town_name 
HAVING Count(people.id) = (SELECT Max(Count(people.id)) doctor_count 
                           FROM   towns 
                                  inner join people 
                                          ON people.postal_code = 
                                             towns.postal_code 
                           WHERE  people.job = 'doctor' 
                           GROUP  BY people.postal_code); 

SELECT town_name AS legkevesebb_doktor_varosa 
FROM   towns 
       left outer join people 
                    ON people.postal_code = towns.postal_code 
                       AND people.job = 'doctor' 
GROUP  BY people.postal_code, 
          towns.town_name 
HAVING Count(people.id) = (SELECT Min(Count(people.id)) doctor_count 
                           FROM   towns 
                                  left outer join people 
                                               ON people.postal_code = 
                                                  towns.postal_code 
                                                  AND people.job = 'doctor' 
                           GROUP  BY towns.postal_code); 

-- 7. Feladat   
SELECT towns.town_name 
FROM   towns 
       inner join pubs 
               ON pubs.town_postal_code = towns.postal_code 
                  AND pubs.capacity < 220 
GROUP  BY towns.postal_code, 
          towns.town_name 
HAVING Count(pubs.pub_id) = (SELECT Max(Count(pubs.pub_id)) pub_cnt 
                             FROM   towns 
                                    inner join pubs 
                                            ON pubs.town_postal_code = 
                                               towns.postal_code 
                                               AND capacity < 220 
                             GROUP  BY towns.postal_code); 

-- 8. Feladat   
SELECT postal_code, 
       address, 
       Listagg(last_name 
               || ' ' 
               || first_name, ', ') 
         within GROUP (ORDER BY(last_name|| ' '|| first_name)) AS people 
FROM   people 
GROUP  BY postal_code, 
          address 
HAVING Count(*) > 1; 

-- 9. Feladat   
SELECT DISTINCT people.id, 
                people.last_name, 
                people.first_name 
FROM   people 
       inner join guests 
               ON guests.person_id = people.id 
       inner join pubs 
               ON pubs.pub_id = guests.pub_id 
WHERE  pubs.pub_name = 'A 7 Mesterlövész' 
ORDER  BY people.id; 

SELECT people.last_name 
       || ' ' 
       || people.first_name 
FROM   people 
WHERE  id IN (SELECT person_id 
              FROM   guests 
                     inner join pubs 
                             ON pubs.pub_id = guests.pub_id 
              WHERE  pubs.pub_name = 'A 7 Mesterlövész'); 

-- 10. Feladat   
SELECT pubs.pub_name 
FROM   people 
       inner join guests 
               ON guests.person_id = people.id 
       inner join pubs 
               ON pubs.pub_id = guests.pub_id 
GROUP  BY pubs.pub_id, 
          pubs.pub_name 
HAVING Count(DISTINCT people.id) = (SELECT 
       Max(Count(DISTINCT people.id)) people_cnt 
                                    FROM   people 
                                           inner join guests 
                                                   ON guests.person_id = 
                                                      people.id 
                                           inner join pubs 
                                                   ON pubs.pub_id = 
                                                      guests.pub_id 
                                    GROUP  BY pubs.pub_id); 
