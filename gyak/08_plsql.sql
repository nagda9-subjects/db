---- PL/SQL

-- Kapcsoljuk be a kimenetet:

set serveroutput on;

-- �ll�tsuk be, hogy a sz�mokat �t lehessen konvert�lni:
ALTER SESSION SET NLS_NUMERIC_CHARACTERS = '.,';

--1. K�sz�ts�k egy hello world nev� programot, ami egy v�ltoz� seg�ts�g�vel ki�rja azt, hogy hello world! (dbms_output.put_line();)

begin
dbms_output.put_line('Hello Vil�g!');
end;
/

--elj�r�sk�nt
 
create or replace procedure hello(nev varchar2)  is
begin
dbms_output.put_line('Hello '|| nev || '!');
end;
/

execute hello('Vil�g');


-- 1.5 azt �rja ki amit beadunk neki

begin
dbms_output.put_line('&beirt_dolog');
end;
/


--2. Hello World 10x

declare
i number:=0;
begin
loop
dbms_output.put_line('Hello Vil�g!');
i:=i+1;
exit when i=10;
end loop;
end;
/

begin
FOR x IN 1..10 LOOP
  dbms_output.put_line('Hello Vil�g!');
END LOOP;
end;
/

declare
i number:=0;
begin
WHILE i<10 
LOOP
dbms_output.put_line('Hello Vil�g!');
i:=i+1;
End Loop;
end;
/


--3. �rassuk ki a p�ros sz�mok n�gyzet�t 1 �s 15 k�z�tt


begin
FOR x IN 1..7 LOOP
  dbms_output.put_line(power(2*x,2));
END LOOP;
end;
/


--4. Iter�ljunk v�gig a verseny t�bl�n egy kurzor seg�ts�g�vel �s �rjuk ki azoknak a versenyz�knek a nev�t �s pontsz�m�t akinek a neve m�sodik bet�je i!

select * from verseny;

-- Ha ez nem ment, akkor l�tre kell m�g hozni a verseny t�bl�t.
-- https://wiki.itk.ppke.hu/twiki/pub/PPKE/AdatbazisKezelesMB2020/VersenyTabla.sql

DECLARE
  CURSOR interaction_cursor is (select * from verseny where versenyzo like '_i%');
BEGIN
  FOR sor in interaction_cursor LOOP
       dbms_output.put_line('n�v: ' || sor.versenyzo|| ' Pontszam: ' || sor.pontszam);
  END LOOP;
END;
/

-- �gy is lehet

-- �gy csak egy elemet �r ki --> kell ide is egy ciklus!
DECLARE
  CURSOR interaction_cursor is select * from verseny where versenyzo like '_i%';
  sor verseny%rowtype;
BEGIN
  OPEN interaction_cursor;
    FETCH interaction_cursor INTO sor;
       dbms_output.put_line('n�v: ' || sor.versenyzo|| ' Pontszam: ' || sor.pontszam);
  CLOSE interaction_cursor;
END;
/

-- ciklussal

DECLARE
  CURSOR interaction_cursor is select * from verseny where versenyzo like '_i%';
  sor verseny%rowtype;
BEGIN
  OPEN interaction_cursor;
  LOOP
    FETCH interaction_cursor INTO sor;
    EXIT WHEN interaction_cursor%NOTFOUND;
       dbms_output.put_line('n�v: ' || sor.versenyzo|| ' Pontszam: ' || sor.pontszam);
  END LOOP;
  CLOSE interaction_cursor;
END;
/

--5. Iter�lj v�gig a versenyt�bl�n �s �rd ki szem�lyenk�nt hogy az adott szem�ly az �tlag pont alatt vagy felett teljes�tett, a nev�t �s a pontsz�m�t!
DECLARE
  CURSOR interaction_cursor is 
  select (select avg(pontszam) from verseny) atlag,pontszam, versenyzo
  from verseny ;
BEGIN
FOR sor in interaction_cursor LOOP
    if sor.pontszam > sor.atlag then
       dbms_output.put_line('�tlagon fel�li: ' || sor.versenyzo|| ' Pontszam: ' || sor.pontszam);
    else
    dbms_output.put_line('�tlag alatti: ' || sor.versenyzo|| ' Pontszam: ' || sor.pontszam);
    end if;
  END LOOP;
END;
/

--6. Iter�lj v�gig a verseny t�bl�n �s ha matekversenyz�t tal�lsz �rd ki, hogy Matek versenyz� + neve + pontsz�ma, ha irodalom versenyz�t tal�lsz �rd ki, hogy Irodalom versenyz�+n�v+pontsz�m , am�gy �rd ki, hogy egy�b tant�rgy versenyz�je +n�v+pontszam
DECLARE
  CURSOR kurzor is 
  select *
  from verseny ;
BEGIN
For sor in kurzor loop
    case lower(sor.tantargy)
        when 'matematika' then dbms_output.put_line('Matek versenyz�: ' || sor.versenyzo|| ' Pontszam: ' || sor.pontszam);
        when 'irodalom' then dbms_output.put_line('Irodalom versenyz� ' || sor.versenyzo|| ' Pontszam: ' || sor.pontszam);
        else dbms_output.put_line('Egy�b tant�rgyb�l versenyz�: ' || sor.versenyzo|| ' Pontszam: ' || sor.pontszam);
    end case;
end loop;
END;
/

--7. Hozzunk l�tre egy f�ggv�nyt, mely kisz�molja egy r sugar� k�r ker�let�t!
create or replace function kerulet(r in number) -- in (megadja, hogy milyen m�dban haszn�ljuk) - k�v�lr�l kap �rt�ket, out: a param�ter arra lesz haszn�lva, hogy visszaadjon vmit a fv-en k�v�l
return number
is 
pi CONSTANT REAL := 3.14159;
BEGIN
return 2*r*pi;
End;
/


--8. megh�vjuk 
begin
dbms_output.put_line('Az 5 sugar� k�r ker�lete: ' || kerulet(5));
end;
/

-- �gy is lehet
select 'Az 5 sugar� k�r ker�lete: ' || kerulet(5)
from dual;



-- 9. F�ggv�ny, ami egy adott tant�rgyhoz ki�rja a tant�rgyban el�rt �tlag pontsz�mot
create or replace function resztvevok_fnct2 (adott_param in varchar2)
return number is
CURSOR interaction_cursor is 
    select tantargy ,avg(pontszam) atlagpont
    from verseny
    group by tantargy;
begin
FOR sor IN interaction_cursor LOOP
if lower(sor.tantargy) like lower(adott_param) then
    return sor.atlagpont;
end if;
END LOOP;
END;
/


begin
dbms_output.put_line('�tlagpontsz�m matematika versenyen: ' || resztvevok_fnct2('irodalom'));
end;
/


--10. K�sz�tsen egy elj�r�st, ami egy adott tant�rgyhoz (bemen� param�ter) ki�rja a versenyz�k nev�t

create or replace procedure resztvevok (adott_param in varchar2) is
CURSOR interaction_cursor is 
    select tantargy, versenyzo 
    from verseny
    where lower(tantargy) like lower(adott_param);-- ha nem tudjuk pontosan beadni a parm�tert: like '%' ||lower(adott_param)||'%'
begin
    dbms_output.put_line('Lek�rt tant�rgy: ' || adott_param ||chr(10)||'Versenyz�k: ');
FOR sor IN interaction_cursor LOOP
    dbms_output.put_line(sor.versenyzo);
END LOOP;
END;
/

begin
resztvevok('Matematika');
end;
/



-- �gy is lehet


create or replace procedure resztvevok_2 (adott_param in varchar2) is
CURSOR interaction_cursor is 
    select tantargy, versenyzo 
    from verseny;
begin
dbms_output.put_line('Lek�rt tant�rgy: ' || adott_param ||chr(10)||'Versenyz�k: ');
FOR sor IN interaction_cursor LOOP
  if lower(sor.tantargy) like lower(adott_param) then
    dbms_output.put_line(sor.versenyzo);
  end if;
END LOOP;
END;
/

begin
resztvevok_2('Matematika');
end;
/



--11. �rjunk egy anonim pl/sql szkriptet, amiben egy v�ltoz�ban megadott t�bla egy m�sik v�ltoz�ban megadott param�tere k�l�nb�z� �rt�keinek sz�m�t k�rdezz�k le.

SET serveroutput ON
DECLARE
  tablanev   VARCHAR2(30);
  oszlopnev  VARCHAR2(30);
  sqlparancs VARCHAR2(500);
  eredmeny   NUMBER;
BEGIN
  tablanev   := 'dcdb.components';
  oszlopnev  := 'molecular_weight';
  sqlparancs := 'select count(distinct ' || oszlopnev || ') from ' || tablanev;
  EXECUTE immediate sqlparancs INTO eredmeny;
  dbms_output.put_line(eredmeny);
END;
/



