-- 1. Feladat. Hozd letre a PATIENT tablat!

DROP TABLE patient;

CREATE TABLE something (
    patient_id            CHAR(5) PRIMARY KEY,
    p_name                VARCHAR2(30) NOT NULL,
    sex                   CHAR(6) CHECK ( sex = 'male'
                        OR sex = 'female' ),
    admission_date        DATE,
    alzheimer_diagnosis   CHAR(40),
    age                   NUMBER
);

ALTER TABLE something RENAME TO patient;

-- 2. Feladat. Toltsetek fel az alabbi adatokkal a korabban definialt tablat!

INSERT INTO patient (
    patient_id,
    p_name,
    sex,
    admission_date,
    alzheimer_diagnosis,
    age
) VALUES (
    'P9700',
    'Clifton Norman',
    'male',
    TO_DATE('02-08-2010', 'dd-mm-yyyy'),
    'severe',
    85
);

INSERT INTO patient (
    patient_id,
    p_name,
    sex,
    admission_date,
    alzheimer_diagnosis,
    age
) VALUES (
    'P1500',
    'Irvin Brody',
    'male',
    TO_DATE('24-10-2004', 'dd-mm-yyyy'),
    'mild',
    46
);

INSERT INTO patient (
    patient_id,
    p_name,
    sex,
    admission_date,
    alzheimer_diagnosis,
    age
) VALUES (
    'P9500',
    'Arden Rodger',
    'female',
    TO_DATE('04-09-2010', 'dd-mm-yyyy'),
    'moderate',
    72
);

INSERT INTO patient (
    patient_id,
    p_name,
    sex,
    admission_date,
    alzheimer_diagnosis,
    age
) VALUES (
    'P4000',
    'Harland Wilbur',
    'male',
    TO_DATE('17-06-2008', 'dd-mm-yyyy'),
    'moderate',
    69
);

INSERT INTO patient (
    patient_id,
    p_name,
    sex,
    admission_date,
    alzheimer_diagnosis,
    age
) VALUES (
    'P8000',
    'Henry Kip',
    'male',
    TO_DATE('28-07-2009', 'dd-mm-yyyy'),
    'severe',
    73
);

COMMIT;

-- 3. Feladat. Milyen recordok vannak a patient tablaban?

SELECT
    *
FROM
    patient;

-- 4. Feladat. Valaszd ki a patient_id, nev es alzheimer_diagnozis oszlopokat!

SELECT
    patient_id,
    p_name,
    alzheimer_diagnosis
FROM
    patient;

-- 5. Feladat. Listazd ki a 70 evesnel oregebb betegek adatait!

SELECT
    *
FROM
    patient p
WHERE
    p.age > 70;

-- 6. Feladat. Mas felhasznalok tablainak elerese.

SELECT
    *
FROM
    stp.patient;

-- 7. Feladat. Listazd ki a betegeket az aktualis eletkorukkal ("age" mezo: eletkor az elso felvetelkor, Aktualis datum: SYSDATE)

SELECT
    p.*,
    p.age + floor((sysdate - p.admission_date) / 365) AS current_age
FROM
    stp.patient p;

-- 8. Feladat. Listazd ki azokat a pacienseket koruk szerint csokkeno, sorrendbe rendezve (azonos kor eseten nevsorban), akik 2005.01.01. utan kerultek felvetelre!

SELECT
    p.*
FROM
    stp.patient p
WHERE
    p.admission_date > TO_DATE('2005.01.01', 'yyyy.mm.dd')
ORDER BY
    p.age DESC,
    p.patient_name;

-- 9. Feladat. Mik az 'ar' vagy 'Ar' karaktereket tartalmazo betegek adatai?

SELECT
    *
FROM
    stp.patient p
WHERE
    p.patient_name LIKE '%ar%'
    OR p.patient_name LIKE '%Ar%';

-- 10. Feladat. Mi az azonositoja es a kora a "Henry" stringgel kezdodo betegeknek?

SELECT
    p.patient_id,
    p.age
FROM
    stp.patient p
WHERE
    p.patient_name LIKE 'Henry%';

-- 11. Feladat. Valasszatok ki azokat a sorokat a treatment tablabol, amelyek cost attributuma nem 'ures'!

SELECT
    *
FROM
    stp.patient p
WHERE
    p.patient_name IS NULL;

SELECT
    *
FROM
    stp.treatment t
WHERE
    t.t_cost IS NOT NULL;

-- 12. Feladat. Valasszatok ki az osszes rekordot, de ugy, hogy a Cost mezoben hianyzo ertekeket 0-val helyettesititek! (Hasznald a nvl() fuggvenyt)

SELECT
    t.treatment_id,
    t.patient_id,
    t.drug,
    nvl(t.t_cost, 0) AS t_cost,
    t.t_time,
    t.consultant
FROM
    stp.treatment t;

-- 13. Feladat. Mit ad vissza a kovetkezo lekerdezes?
  -- Azon rekordokat, ahol a T_COST mezo erteke nem NULL:

SELECT
    *
FROM
    stp.treatment t
WHERE
    t.t_cost < 60
    OR t.t_cost >= 60;
-- a < �s >= kisz�ri a null �rt�keket