drop table allinfo;

create table allinfo(
ID varchar2(255),
name varchar2(255),
salary number,
dept_name varchar2(255),
building varchar2(255),
budget number
);

insert into allinfo values('22222','Einstein',95000,'Physics','Watson',70000);
insert into allinfo values('12121','Wu',90000,'Finance','Painter',120000);
insert into allinfo values('32343','El Said',60000,'History','Painter',50000);
insert into allinfo values('45565','Katz',75000,'Comp. Sci.','Taylor',100000);
insert into allinfo values('98345','Kim',80000,'Elec. Eng','Taylor',85000);
insert into allinfo values('76766','Crick',72000,'Biology','Watson',90000);
insert into allinfo values('10101','Srinivasan',65000,'Comp. Sci.','Taylor',100000);
insert into allinfo values('58583','Califieri',62000,'History','Painter',50000);
insert into allinfo values('83821','Brandt',92000,'Comp. Sci.','Taylor',100000);
insert into allinfo values('15151','Mozart',40000,'Music','Packard',80000);
insert into allinfo values('33456','Gold',87000,'Physics','Watson',70000);
insert into allinfo values('76543','Singh',80000,'Finance','Painter',120000);

