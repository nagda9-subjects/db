-- 1. Feladat: Irasd ki a dolgozok fizetesenek futo osszeget! Rendezd a dolgozokat nevsorba!
SELECT
    last_name,
    first_name,
    salary,
    SUM(salary) OVER(ORDER BY last_name, first_name) running_total
FROM
    hr.employees
ORDER BY
    last_name,
    first_name;
	
-- 2. Feladat: Irasd ki a dolgozok fizetesenek futo osszeget! 
-- Rendezd a dolgozokat fizetesuk szerint csokkeno, azon belul pedig nevsorba!
SELECT
    last_name,
    first_name,
    salary,
    SUM(salary) OVER(ORDER BY salary DESC, last_name, first_name) running_total
FROM
    hr.employees
ORDER BY
    salary DESC, last_name, first_name;
	
-- 3. Feladat: Irasd ki az atlag eletkort varosonkent (iranyito szam)!

SELECT
    postal_code,
    round(AVG(floor( (SYSDATE - date_of_birth) ) / 365),1) AS atlag_kor
FROM
    pptg.people
GROUP BY
    postal_code;

SELECT
    first_name,
    last_name,
    postal_code,
    round(AVG(round( (SYSDATE - date_of_birth) ) / 365) OVER(PARTITION BY postal_code) ) AS atlag_kor
FROM
    pptg.people
ORDER BY
    last_name,
    first_name;

-- 4. Feladat: Irasd ki, hogy az emberek eletkora mennyire ter el a varosuk atlageletkoratol!
SELECT
    p.first_name,
    p.last_name,
    p.postal_code,
    round( (SYSDATE - date_of_birth) / 365) kor,
    atl.atlag_kor,
    atlag_kor - round( (SYSDATE - date_of_birth) / 365) elteres
FROM
    pptg.people p
    INNER JOIN (
        SELECT
            postal_code,
            round(AVG(round( (SYSDATE - date_of_birth) ) / 365) ) AS atlag_kor
        FROM
            pptg.people
        GROUP BY
            postal_code
    ) atl ON p.postal_code = atl.postal_code;

SELECT
    first_name,
    last_name,
    postal_code,
    round(AVG(round( (SYSDATE - date_of_birth) ) / 365) OVER(PARTITION BY postal_code) ) AS atlag_kor,
    round( (SYSDATE - date_of_birth) / 365) kor,
    round(AVG(round( (SYSDATE - date_of_birth) ) / 365) OVER(PARTITION BY postal_code) ) 
		- round( (SYSDATE - date_of_birth) / 365) AS elteres
FROM
    pptg.people
ORDER BY
    last_name,
    first_name;

-- 5. Feladat: Irasd ki, hogy mekkora volt az osszes latogatás az egyes napokon, es ez mennyivel volt tobb, mint az elozo napon!

SELECT
    date_of_visit,
    COUNT(guest_id) latogatasok_szama,
    LAG(COUNT(guest_id),1,NULL) OVER(ORDER BY date_of_visit) elozo_nap,
    LEAD(COUNT(guest_id),1,NULL) OVER(ORDER BY date_of_visit) kovetkezo_nap,
    COUNT(guest_id) - LAG(COUNT(guest_id),1,NULL) OVER(ORDER BY date_of_visit) novekedes
FROM
    pptg.guests
GROUP BY
    date_of_visit
ORDER BY
    date_of_visit;

-- 6. Feladat: Irasd ki az elso harom legidosebb embert varosonkent!

SELECT *
FROM
    (  	SELECT
            last_name || ' ' || first_name name,
            postal_code,
            ROW_NUMBER() OVER(PARTITION BY postal_code ORDER BY date_of_birth ASC) r_num
        FROM
            pptg.people )
WHERE
    r_num < 4;

SELECT
    postal_code,
    LISTAGG(r_num || ': ' || name,', ') WITHIN GROUP(ORDER BY r_num) emberek
FROM
    (   SELECT
            last_name || ' ' || first_name name,
            postal_code,
            ROW_NUMBER() OVER(PARTITION BY postal_code ORDER BY date_of_birth ASC) r_num
        FROM
            pptg.people )
WHERE
    r_num < 4
GROUP BY
    postal_code;

-- 7. Feladat: Irasd ki az elso harom legregebbi evben szuletett embert varosonkent! (to_char(date_of_birth, 'yyyy'))

select *
from
  (select
    LAST_NAME || ' ' || FIRST_NAME name,
    postal_code,
    row_number() over(PARTITION BY postal_code order by to_char(date_of_birth, 'yyyy') asc) r_num,
    rank() over(PARTITION BY postal_code order by to_char(date_of_birth, 'yyyy') asc) r_rank,
    dense_rank() over(PARTITION BY postal_code order by to_char(date_of_birth, 'yyyy') asc) r_dense_rank
  from people)
where r_dense_rank < 4;

-- 8. Írasd ki, hogy egy adott vendég által látogatott kocsma forgalma 
-- hogyan alakult a látogatástól számított 12 órában.

SELECT
    last_name || ' ' || first_name name,
    pub_id,
    date_of_visit,
    COUNT(person_id) OVER(PARTITION BY pub_id ORDER BY date_of_visit
        RANGE BETWEEN CURRENT ROW AND INTERVAL '12' HOUR FOLLOWING) latogatasokszama
FROM
    pptg.people p
    INNER JOIN pptg.guests g ON p.id = g.person_id
ORDER BY
    pub_id,
    date_of_visit,
    name;

-- 9. Írasd ki, hogy egy adott kocsmába látogató, 
-- egymást követő három embernek mennyi az átlagéletkora.

SELECT
    last_name || ' ' || first_name name,
    pub_id,
    date_of_visit,
    round(AVG(floor( (SYSDATE - date_of_birth) / 365) ) OVER(PARTITION BY pub_id
        ORDER BY date_of_visit ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING),1) atlageletkor
FROM
    pptg.people p
    INNER JOIN pptg.guests g ON p.id = g.person_id
ORDER BY
    pub_id,
    date_of_visit,
    name;

-- 10. Feladat: Ird ki tantargyankent az atlagpontokat!

SELECT
    tantargy,
    round(AVG(pontszam) )
FROM
    verseny
GROUP BY
    tantargy

-- 11. Feladat: Ird ki, hogy ki hany ponttal ter el az atlagtol (tantargyankent)!

SELECT
    tantargy,
    pontszam - round(AVG(pontszam) OVER(PARTITION BY tantargy) ) atlagpont
FROM
    verseny;

-- 12. Feladat: Ird ki tantargyankent az elso 3 helyezettet!

SELECT *
FROM   (SELECT versenyzo,
               tantargy,
               pontszam,
               Row_number()
                 over (
                   PARTITION BY tantargy
                   ORDER BY pontszam DESC) sorszam,
               Rank()
                 over (
                   PARTITION BY tantargy
                   ORDER BY pontszam DESC) rangsor,
               Dense_rank()
                 over (
                   PARTITION BY tantargy
                   ORDER BY pontszam DESC) suru_rangsor
        FROM   verseny)
WHERE  suru_rangsor < 4
ORDER  BY tantargy,
          pontszam DESC;

--listagg
SELECT tantargy, listagg(suru_rangsor||': '||versenyzo||' '||pontszam, ', ') within group(order by pontszam desc) elso_harom
FROM   (SELECT versenyzo,
               tantargy,
               pontszam,
               Row_number()
                 over (
                   PARTITION BY tantargy
                   ORDER BY pontszam DESC) sorszam,
               Rank()
                 over (
                   PARTITION BY tantargy
                   ORDER BY pontszam DESC) rangsor,
               Dense_rank()
                 over (
                   PARTITION BY tantargy
                   ORDER BY pontszam DESC) suru_rangsor
        FROM   verseny)
WHERE  suru_rangsor < 4
group by tantargy;

--13. Feladat: Ird ki, hogy ki hany ponttal maradt le az eggyel jobb helyezettol (targyankent)!
SELECT versenyzo,
       tantargy,
       pontszam,
       Lag(pontszam, 1)
         over (
           PARTITION BY tantargy
           ORDER BY pontszam DESC) eggyel_jobb_pontszama,
       Nvl(( Lag(pontszam, 1)
               over (
                 PARTITION BY tantargy
                 ORDER BY pontszam DESC) ) - pontszam, 0) lemaradas
FROM   verseny
ORDER  BY tantargy,
          pontszam DESC;
          


