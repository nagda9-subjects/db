﻿drop table number_table;
drop table duplicate;
drop table t4;
drop table t5;
drop table mylog;
drop table person;
drop table emp;

-- Kell, hogy láthassuk a PL/SQL kódok kimenetét.
set serveroutput on;

-- Ciklusok példái

-- Segédtábla létrehozása
create table number_table(
	num NUMBER(10)
);

-- "Sima" ciklus - LOOP
DECLARE
  i  number_table.num%TYPE := 1;
BEGIN
  LOOP
    INSERT INTO number_table
      VALUES(i);
    i := i + 1;
    EXIT WHEN i > 10;
  END LOOP;
END;
/

-- Segédtábla létrehozása
create table duplicate(
	num NUMBER(10)
);

-- CURSOR FOR ciklus (fix lépésszámú)
DECLARE
	CURSOR c IS
		SELECT * FROM number_table;
BEGIN
	FOR num_row IN c LOOP
       INSERT INTO duplicate 
		VALUES(num_row.num*10);
	END LOOP; 
END;
/

SELECT * FROM duplicate;

-- Kivételkezelés

-- User defined
DECLARE 
	e_number1  EXCEPTION;
	cnt        NUMBER;
BEGIN
	SELECT count(*) INTO cnt
	FROM number_table;
	IF cnt = 1 THEN RAISE e_number1;
	ELSE dbms_output.put_line(cnt);
	END IF;
EXCEPTION
	WHEN e_number1 THEN
	dbms_output.put_line('Count = 1');
end;
/

-- Eljárások

-- Segédtábla létrehozása és feltöltése adatokkal
CREATE TABLE mylog
   (	who VARCHAR2(10), 
		logon_num NUMBER);
		
Insert into ZSETA.MYLOG (WHO,LOGON_NUM) values ('Pete','3');
Insert into ZSETA.MYLOG (WHO,LOGON_NUM) values ('John','4');
Insert into ZSETA.MYLOG (WHO,LOGON_NUM) values ('Joe','4');

-- Eljárás, ami lekérdezi egy adott emberről, hogy hányszor jelentkezett be
-- Név: num_logged
-- Paraméterek:
-- 	person - a lekérdezni kívánt ember neve, bemeneti paraméter, a típusa egyezik a mylog tábla who attribútumának típusával
-- 	num - a tárolt belépések száma, kimeneti paraméter, a típusa egyezik a mylog tábla logon_num attribútumának típusával

create or replace procedure 
num_logged
(person IN mylog.who%TYPE,
 num OUT mylog.logon_num%TYPE)
IS
BEGIN
    select logon_num 
    into num
    from mylog
    where who = person;
END;
/

-- Példa arra, hogy hogyan lehet meghívni az eljárást.
-- Az előadásban megbeszéltük: Milyen hibák léphetnek fel? 
DECLARE
    howmany  mylog.logon_num%TYPE;
BEGIN
    num_logged('John',howmany);
    dbms_output.put_line('John logged in ' 	|| howmany || ' times.');
END;
/

-- Függvények

-- Szám négyzetét visszaadó függvény
-- Név: squareFunc
-- Paraméterek:
-- 	num - a szám, aminek a négyzetét számoljuk, bemeneti paraméter, a típusa NUMBER
--	visszatérési értékének típusa NUMBER
-- Függvény esetén szükséges a RETURN klóz használata a visszatérési érték típusának megadására

CREATE OR REPLACE FUNCTION squareFunc(num IN NUMBER) 
RETURN NUMBER IS 
BEGIN
RETURN num*num;
End;
/

-- Függvény hívása anoním blokkból
BEGIN
dbms_output.put_line(squareFunc(3.5));
END;
/

-- Függvény hívása a beépített dual tábla segítségével
SELECT squareFunc(3.5)
FROM dual;

-- Triggerek
-- Olyan PL/SQL kódok, amik az adatbázisban bekövetkező események hatására futnak le, amire be vannak állítva

-- Log trigger - egy táblán (vagy más objektumon) bekövetkezett eseményt naplózunk vele

-- Segédtáblák létrehozása
-- A T4 táblán fogjuk figyeltetni, hogy mi történik
CREATE TABLE T4 
( a INTEGER, b CHAR(10));
-- A T5 táblába kerülnek a log "bejegyzések"
CREATE TABLE T5 (c CHAR(10), d INTEGER);

-- Trigger létrehozása
-- Név: trig1
-- Működés: A T4 táblába való beillesztés hatására, ha a beillesztett szám kisebb, mint tíz, tegyünk egy bejegyzést a T5 táblába.
CREATE OR REPLACE TRIGGER trig1
--	A T4 táblába való beillesztés után fog lefutni
AFTER INSERT ON T4
--	A beillesztésre került sort (NEW) newRow néven hivatkozzuk a kódban
REFERENCING NEW AS newRow
--	Sorszintű trigger a "FOR EACH ROW" miatt
--	Akkor fut, ha a beillesztett szám kisebb, mint tíz
FOR EACH ROW WHEN (newRow.a <= 10)
BEGIN
-- Beillesztjük a T5 táblába az imént a T4 táblába beillesztett értékeket.
	INSERT INTO T5 VALUES (:newRow.b, :newRow.a);
END trig1;
-- Hibát ad, ezt javítani kell (hibakeresés példa)
/

-- Próbáljuk ki a trig1 működését, illesszünk adatokat a T4 táblába!
insert into T4 values(4, 'kicsi');
insert into T4 values(3, 'kicsi');
insert into T4 values(20, 'nagy');
-- A T5-be a 10-nél kisebb számok esetén kerül be új sor.
select * from T5;

-- Értéktartomány ellenőrzés saját hibaüzenettel

-- Segédtábla létrehozása
CREATE TABLE Person 
(name varchar(20), age int);

-- Trigger létrehozása
-- Név: PersonCheckAge
-- Működés: Nem szeretnénk, hogy a Person tábla age mezőjében negatív érték lehessen. 
--	Ebben az esetben egy saját hibaüzenettel térjen vissza.
CREATE OR REPLACE TRIGGER PersonCheckAge
--	A Person táblába való beillesztés, vagy a táblán való frissítés előtt fog lefutni
BEFORE INSERT OR UPDATE OF age ON Person 
--	Sorszintű trigger a "FOR EACH ROW" miatt
FOR EACH ROW 
BEGIN 
-- Nem neveztük át a beillesztésnél, vagy frissítésnél használni kívánt sort, így az alapértelmezett :new változóval hivatkozhatjuk
	IF(:new.age < 0)	THEN
-- Hibát dobunk, amihez kell egy hibakód (Oracle dokumentáció írja, hogy milyen érték lehet), valamint egy szöveges üzenet.
	RAISE_APPLICATION_ERROR(-20000, 'No negative age is allowed!'); 
-- Természetesen hiba esetén nem történik meg az insert/update
	END IF; 
END;
/

-- Próbáljuk ki a PersonCheckAge működését!
insert into Person values('Abel', 10);
insert into Person values('Gideon', 178);
insert into Person values('Noah', 0);
insert into Person values('Job', -3);
update Person set age=age+2 where lower(name) like '%abel%';
update Person set age=age-2 where lower(name) like '%noah%';


-- Képernyőre adatokat kiíró trigger
-- Segédtábla létrehozása
create table emp as select employee_id as id, first_name || ' ' || last_name as name, phone_number, salary, 
commission_pct, manager_id, department_id from HR.EMPLOYEES;

-- Az emp táblában szeretnénk nyomonkövetni a beillesztés, törlés, frissítés műveleteket.
-- Trigger létrehozása
-- Név: print_salary_changes
CREATE OR REPLACE TRIGGER print_salary_changes
-- Mivel nem változtatunk az értéken, itt akár AFTER is lehet
BEFORE DELETE OR INSERT OR UPDATE ON emp
-- Sor szintű trigger kell, mivel az egyes soroknál található információra szükségünk van
FOR EACH ROW 
-- *1* Kell-e feltétlenül ez az ellenőrzés? Mi lesz a hatása?
WHEN (new.id > 0)
DECLARE 
	sal_diff number; 
BEGIN 
	sal_diff := :new.salary - :old.salary; 
	dbms_output.put('Old salary: ' ||:old.salary); 
	dbms_output.put(' New salary: ' ||:new.salary); 
	dbms_output.put_line(' Difference: '||sal_diff); 
END;
/

-- Trigger kipróbálása
-- Itt trükközni kell egy kicsit az SQLDeveloper miatt - nem mindig olvassa ki a serveroutput bufferben található sorokat
-- Akkor már tegyük mellé azt is, hogy hány soron végeztük el az adott műveletet
DECLARE   
   total_rows number(20);  
BEGIN  
-- Frissítés tesztelése
   UPDATE  emp  
   SET salary = salary - 5000;
-- Ha nem futott le, akkor mást írjunk ki; az sql kurzorban (ami automatikusan létrejön) turjuk ellenőrizni 
-- az utasításunk által érintett sorok számát
-- vö: https://docs.oracle.com/en/database/oracle/oracle-database/12.2/lnpls/static-sql.html#GUID-87C3CAFD-625D-4785-A32F-14E2440335DC
   IF sql%notfound THEN  
      dbms_output.put_line('no employee updated');  
   ELSIF sql%found THEN  
      total_rows := sql%rowcount;  
      dbms_output.put_line( total_rows || ' employee(s) updated ');  
   END IF;
-- Beszúrás tesztelése (több sort illesztünk be, hogy itt is működjön a számolás)
-- vö: https://docs.oracle.com/en/database/oracle/oracle-database/12.2/sqlrf/INSERT.html#GUID-903F8043-0254-4EE9-ACC1-CB8AC0AF3423
   insert all
   into emp values('989','Test User','',99999,'','','')
   into emp values('999','Test User2','',1,'','','')
   SELECT * FROM dual;
   IF sql%notfound THEN  
      dbms_output.put_line('no employee inserted');  
   ELSIF sql%found THEN  
      total_rows := sql%rowcount;  
      dbms_output.put_line( total_rows || ' employee(s) inserted ');  
   END IF;
-- Törlés tesztelése
-- Miért nem írja ki itt az értékeket? vö: *1*
   delete emp where id>980;
   IF sql%notfound THEN  
      dbms_output.put_line('no employee deleted');  
   ELSIF sql%found THEN  
      total_rows := sql%rowcount;  
      dbms_output.put_line( total_rows || ' employee(s) deleted ');  
   END IF;
END;  
/  

