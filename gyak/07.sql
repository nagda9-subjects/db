-- 1. Feladat: Irasd ki csoportonkent a megosztott audiofajlok szamat, 
-- az eredmenyt rendezd utobbi szerint csokkeno sorrendbe!
SELECT
    groups.id group_id,
    groups.name group_name,
    COUNT(shares.id) audio_count
FROM
    mb18___db.groups groups
    LEFT OUTER JOIN mb18___db.shares shares ON groups.id = shares.group_id
GROUP BY
    groups.id,
    groups.name
ORDER BY
    audio_count DESC;
	
-- 2. Feladat: Egeszitsd ki az elozo feladatot azzal, hogy csoportonkent 
-- lathato legyen a fajlok ossz-, illetve atlagos hossza (az eredmeny legyen kerekitve, 
-- a NULL ertekeket 0-val helyettesitsd)! (Segitseg: az audio tabla duration oszlopat hasznald.)
SELECT
    groups.id group_id,
    groups.name group_name,
    COUNT(shares.id) audio_count,
    nvl(SUM(audio.duration),0) total_length,
    nvl(round(AVG(audio.duration) ),0) average_length
FROM
    mb18___db.groups groups
    LEFT OUTER JOIN mb18___db.shares shares ON groups.id = shares.group_id
    LEFT OUTER JOIN mb18___db.audio audio ON audio.id = shares.audio_id
GROUP BY
    groups.id,
    groups.name
ORDER BY
    audio_count DESC;

-- 3. Feladat: Egeszitsd ki az elozo lekerdezest egy ujabb oszloppal, ami megmutatja, 
-- hogy hany tagja van az adott csoportnak! (Segitseg: hasznald a members tablat.)
-- Fan trap feladat: 1 csoporthoz tobb megosztas es tobb tag tartozhat. Igy egy olyan sor, 
-- amiben 1 csoport 1 megosztasa van, annyiszor fog szerepelni, ahany tagja van a csoportnak
-- (igy osszesen megosztas�tag darab sor tartozik majd 1 csoporthoz).
SELECT
    group_stats.group_id,
    group_stats.group_name,
    group_stats.audio_count,
    group_stats.total_length,
    group_stats.average_length,
    COUNT(members.group_id) member_count
FROM
    (
        SELECT
            groups.id group_id,
            groups.name group_name,
            COUNT(shares.id) audio_count,
            nvl(SUM(audio.duration),0) total_length,
            nvl(round(AVG(audio.duration) ),0) average_length
        FROM
            mb18___db.groups groups
            LEFT OUTER JOIN mb18___db.shares shares ON groups.id = shares.group_id
            LEFT OUTER JOIN mb18___db.audio audio ON audio.id = shares.audio_id
        GROUP BY
            groups.id,
            groups.name
    ) group_stats
    LEFT OUTER JOIN mb18___db.members members ON members.group_id = group_stats.group_id
GROUP BY
    group_stats.group_id,
    group_stats.group_name,
    group_stats.audio_count,
    group_stats.total_length,
    group_stats.average_length;
	
-- 4. Feladat: Gyujtsd ki, hogy hany felhasznalo regisztralt az adott honapokban 
-- (eleg azokat a honapokat, amikor volt regisztracio)! Az eredmenyt rendezd! 
-- (Segitseg: hasznald a users tabla created_at oszlopat.) 
SELECT
    TO_CHAR(created_at,'yyyy-mm') reg_month,
    COUNT(*) count
FROM
    mb18___db.users
GROUP BY
    TO_CHAR(created_at,'yyyy-mm')
ORDER BY
    reg_month;
	
-- 5. Feladat: Ird ki azokat a csoportokat amelyeknek legalabb 10 tagjuk van!
SELECT
    group_id,
    COUNT(user_id) tagok_szama
FROM
    mb18___db.members
GROUP BY
    group_id
HAVING
    COUNT(user_id) >= 10;

-- 6. Feladat: Ird ki a csoportok nevet, a csoport tagjainak szamat, 
-- illetve a csoport altal megosztott audiok szamat! Azok a csoportok is legyenek az eredmenyben, 
-- akik nem osztottak meg audiot, illetve nincs egy tagjuk sem!

-- 6_1. Reszfeladat: Kerdezd le csoportokat es az altaluk megosztott audiok szamat! 
-- Kezeld megfeleloen ha egy csoport nem osztott meg audiot! 
SELECT
    g.id id,
    g.name csoport_nev,
    COUNT(audio_id) audio_szama
FROM
    mb18___db.groups g
    LEFT OUTER JOIN mb18___db.shares s ON g.id = s.group_id
GROUP BY
    g.id,
    g.name;
	
-- 6_2. Reszfeladat: Kerdezd le csoportokat es a tagjaik szamat! 
-- Kezeld megfeleloen ha egy csoportnak nincsenek tagjai! 
SELECT
    g.id,
    COUNT(m.user_id) tagok_szama
FROM
    mb18___db.groups g
    LEFT OUTER JOIN mb18___db.members m ON m.group_id = g.id
GROUP BY
    g.id;
	
-- Megoldas:
SELECT
    csoport_nev,
    COUNT(m.user_id) tagok_szama,
    audio_szama
FROM
    (
        SELECT
            g.name csoport_nev,
            g.id id,
            COUNT(audio_id) audio_szama
        FROM
            mb18___db.groups g
            LEFT OUTER JOIN mb18___db.shares s ON g.id = s.group_id
        GROUP BY (
            g.name,
            g.id
        )
    ) t1
    LEFT OUTER JOIN mb18___db.members m ON m.group_id = t1.id
GROUP BY
    csoport_nev,
    audio_szama;

-- 7. Feladat: Listazd ki csoportonkent a bejelentkezeseket (LAST_LOGIN_AT), 
-- illetve azt hogy az adott bejelentkezes mennyivel kovette az azt megelozo bejelentkezest. 
-- Az eredmenyt rendezd a csoporton beluli bejelentkezesek szama szerinti csokkeno sorrendbe! 
-- Csak azok a csoportok szerepeljenek a vegeredmenyben, ahol tortent bejelentkezes!

-- 7_1. Reszfeladat: Listazd ki a csoportokat a legutobbi bejelentkezeseket illetve az azt megelozo
-- bejelentkezest (csoportonkent, novekvo sorrendben)! Csak azok a csoportok szerepeljenek az
-- eredmenyben amelyekben tortent bejelentkezes!
SELECT
    group_id,
    last_login_at,
    LAG(last_login_at) OVER(
        PARTITION BY group_id
        ORDER BY
            last_login_at ASC
    ) elozo_login
FROM
    mb18___db.members m
    INNER JOIN mb18___db.users u ON m.user_id = u.id
ORDER BY
    last_login_at ASC;

-- Megoldas:
SELECT
    group_id,
    last_login_at last_login_at,
    last_login_at - ( LAG(last_login_at) OVER(
        PARTITION BY group_id
        ORDER BY
            last_login_at ASC
    ) ) eltelt_ido
FROM
    mb18___db.members m
    INNER JOIN mb18___db.users u ON m.user_id = u.id
ORDER BY
    COUNT(last_login_at) OVER(
        PARTITION BY group_id
    ) DESC,
    last_login_at DESC;

-- 8. Feladat: Listazd ki azokat az audiokat cimmel es szerzovel, 
-- amelyeket 2013 oktober 1 es 2014 februar 1 kozott bejelentkezett felhasznalok hallgattak, 
-- hosszabbak, mint az osszes audio atlag hossza, illetve a szerzo neveben van 'w' betu (kicsi vagy nagy)!

-- 8_1. Reszfeladat: Listazd ki azokat a szerzoket akiknek a neve tartalmaz 'w' betut! (kicsi vagy nagy)!
SELECT
    author
FROM
    mb18___db.audio
WHERE
    lower(author) LIKE '%w%';

-- 8_2. Reszfeladat: Listazd ki azokat a felhasznalokat, akik 2013 oktober 1 es 2014 februar 1 kozott jelentkeztek be!
SELECT
    id,
    last_login_at
FROM
    mb18___db.users
WHERE
    last_login_at >= TO_DATE('2013-10-01','YYYY-MM-DD')
    AND   last_login_at <= TO_DATE('2014-02-01','YYYY-MM-DD'); 

-- 8_3. Reszfeladat: Listazd ki azokat az audiokat, amelyek rovidebbek, mint az osszes audio atlagos hossza! 
SELECT
    title,
    duration
FROM
    mb18___db.audio
WHERE
    duration < (
        SELECT
            AVG(duration)
        FROM
            mb18___db.audio
    );
	
-- Megoldas:

SELECT DISTINCT
    t1.author,
    t1.title
FROM
    (
        SELECT
            id,
            title,
            author
        FROM
            mb18___db.audio
        WHERE
            duration > (
                SELECT
                    AVG(duration)
                FROM
                    mb18___db.audio
            )
            AND   lower(author) LIKE '%w%'
    ) t1
    INNER JOIN mb18___db.historyitems t2 ON t1.id = t2.audio_id
    INNER JOIN mb18___db.users u ON t2.user_id = u.id
WHERE
    last_login_at >= TO_DATE('2013-10-01','YYYY-MM-DD')
    AND   last_login_at <= TO_DATE('2014-02-01','YYYY-MM-DD');

-- Vagy igy is jo:

SELECT
    title,
    author
FROM
    mb18___db.audio       a
    INNER JOIN mb18___db.playitems   p ON p.audio_id = a.id
    INNER JOIN mb18___db.users       u ON u.id = p.user_id
WHERE
    u.last_login_at BETWEEN TO_DATE('2013.10.01', 'yyyy.mm.dd') AND TO_DATE('2014.02.01', 'yyyy.mm.dd')
    AND duration > (SELECT AVG(duration) FROM mb18___db.audio)
    AND lower(author) LIKE '%w%';
	



-- 9. Feladat: Kerdezd le a historyitems tablabol a lejatszasok kezdetet (started_at), 
-- hosszat (duration) es a kettobol szamolt veget!
SELECT
    TO_CHAR(started_at,'YYYY-mm-DD HH:MI:SS') AS kezdes,
    duration AS idotartam,
    TO_CHAR(duration / 86400 + started_at,'YYYY-mm-DD HH:MI:SS') AS befejezes
FROM
    mb18___db.historyitems; 

-- 10. Feladat: Egeszitsd ki az elozo lekerdezest egy oszloppal, ami az elozo lejatszas vege es 
-- az aktualis lejatszas eleje kozott eltelt idot irja ki felhasznalonkent, masodpercben merve. 
-- Az eredmeny legyen sorba rendezve a felhasznalok es azon belul a lejatszasi idok szerint.
-- Felhasznalonkent az elso sorban NULL ertek szerepeljen az uj oszlopban. 
SELECT
    id,
    user_id,
    TO_CHAR(started_at,'YYYY-MM-DD HH24:MI:SS') started_at_formatted,
    TO_CHAR(started_at + duration / 86400,'YYYY-MM-DD HH24:MI:SS') stopped_at_formatted,
    duration,
    started_at - LAG(started_at + duration / 86400) OVER(
        PARTITION BY user_id
        ORDER BY
            started_at
    ) gap_unformatted
FROM
    mb18___db.historyitems
ORDER BY
    user_id,
    started_at;

SELECT
    id,
    user_id,
    TO_CHAR(started_at,'YYYY-MM-DD HH24:MI:SS') started_at_formatted,
    TO_CHAR(stopped_at,'YYYY-MM-DD HH24:MI:SS') stopped_at_formatted,
    duration,
    started_at - LAG(stopped_at) OVER(
        PARTITION BY user_id
        ORDER BY
            started_at
    ) gap_unformatted
FROM
    (
        SELECT
            id,
            user_id,
            started_at,
            duration,
            started_at + duration / 86400 AS stopped_at
        FROM
            mb18___db.historyitems
    ) t
ORDER BY
    user_id,
    started_at;
