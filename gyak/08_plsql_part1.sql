﻿drop table emp;
drop table number_table;
drop table duplicate;

-- Kell, hogy láthassuk a PL/SQL kódok kimenetét.
set serveroutput on;

begin
DBMS_OUTPUT.PUT_LINE('Hello');
end;
/

select * from HR.EMPLOYEES;

create table emp
as (
	select 	employee_id as id, 
			First_name || ' ' || last_name as name, 
			phone_number, 
			salary, 
			commission_pct,
			manager_id,
			department_id
	from HR.EMPLOYEES);

DECLARE
  	v_hold_one_row		Emp%ROWTYPE;
	v_name				Emp.name%TYPE;
	v_fav_emp			VARCHAR2(30);
begin
	-- A következő sort kommentezni kell, hogy lefusson.
	v_hold_one_row := 987; -- Ez nem fog így működni, mivel az emp tábla egy sorát tárolja a változó.
	v_hold_one_row.id := 877; -- Itt csak az egyik attribútumnak adunk értéket.
end;
/

-- SQL beágyazás bemutatása - insert, update, delete
DECLARE
    emp_id emp.id%TYPE;
    emp_name emp.name%TYPE;
BEGIN
    emp_id := 299;
    INSERT INTO emp(id,name) VALUES(emp_id,'Bob Henry');
    UPDATE emp
        set name = 'Robert Henry' WHERE id = emp_id;
    DELETE FROM emp where id = 299
        RETURNING name
            INTO emp_name;
    COMMIT;
    DBMS_OUTPUT.PUT_LINE('Deleted: ' || emp_name);
END;
/

-- SQL beágyazás bemutatása - Select into, alap hibák (pontosan egy értéknek kell a végeredményben lennie)
DECLARE
  v_ename	 Emp.name%TYPE;
  v_id	 Emp.id%TYPE;
BEGIN
  SELECT	name, id
    INTO	v_ename, v_id
    FROM	emp
   WHERE	id = 112;
   -- WHERE	id = 999; -- No data
   -- WHERE	name like '%a%'; -- Too many rows
   DBMS_OUTPUT.PUT_LINE('Employee: ' || v_ename || ' has id: ' || v_id || '.');
END;
/

-- Erről következő órán lesz szó (ez az eljárás, amit a következő példa használ)
create or replace procedure assign_bonus (emp_id IN emp.id%TYPE, total IN OUT INTEGER)
is
    emp_sal emp.salary%TYPE;
begin
    SELECT salary INTO emp_sal FROM emp WHERE id = emp_id;
    UPDATE emp
        set salary = emp_sal + 500 WHERE id = emp_id;
    total := total - 500;
end;
/
commit;

-- Több sor kezelése -> kurzor
-- Adjunk bónuszt a kevesebbet kereső dolgozóknak, erre van 10000 keretünk.
DECLARE
    l_total    INTEGER := 10000; -- kiosztható pénzösszeg
    CURSOR emp_id_cur IS -- több sort várunk az eredményben
        SELECT  id
        FROM    emp
        ORDER BY    salary ASC;
    l_emp_id   emp_id_cur%rowtype; -- a sorokat egyesével tudjuk feldolgozni, ehhez kell egy változó, amibe betesszük
BEGIN
    OPEN emp_id_cur; -- kurzor nyit
    LOOP -- ciklussal végigmegyünk a kapott sorokon
        FETCH emp_id_cur INTO l_emp_id; -- következő sor kiolvasása
        EXIT WHEN emp_id_cur%notfound; -- kilépünk, ha már nincs több adat
        assign_bonus(l_emp_id.id,l_total); -- bónuszt osztunk, egyúttal csökkentjük a kiosztható pénzösszeget
        EXIT WHEN l_total <= 0; -- kilépünk, ha már elfogyott a pénz
    END LOOP;
    CLOSE emp_id_cur; -- kurzor zár
END;
/

-- Ciklusok példái

-- Segédtábla létrehozása
create table number_table(
	num NUMBER(10)
);

-- "Sima" ciklus - LOOP
DECLARE
  i  number_table.num%TYPE := 1;
BEGIN
  LOOP
    INSERT INTO number_table
      VALUES(i);
    i := i + 1;
    EXIT WHEN i > 10;
  END LOOP;
END;
/

-- Segédtábla létrehozása
create table duplicate(
	num NUMBER(10)
);

-- "Sima" ciklus kurzorral
DECLARE
  cursor c is select * from number_table;
  cVal c%ROWTYPE;--cursor type!!
BEGIN
  open c;
  LOOP
    fetch c into cVal;
    EXIT WHEN c%NOTFOUND;
    insert into duplicate values(cVal.num*2);
  END LOOP;
  close c;
END;
/

-- FOR ciklus (fix lépésszámú)
DECLARE
  i		number_table.num%TYPE;
BEGIN
  FOR i IN 1..10 LOOP
    INSERT INTO number_table VALUES(i);
  END LOOP;
END;
/

-- Sorban irassuk ki a főnököket!
DECLARE
    v_ename   emp.name%TYPE;
    CURSOR c_managers IS 
        SELECT  name
        FROM    emp
        WHERE   id IN (SELECT manager_id FROM emp);
BEGIN
    dbms_output.put_line('Managers:');
    OPEN c_managers;
    LOOP
        FETCH c_managers INTO v_ename;
        EXIT WHEN c_managers%NOTFOUND;
		dbms_output.put_line(v_ename);
    END LOOP;
END;
/

-- WHILE ciklus
DECLARE
TEN number:=10;
i	number_table.num%TYPE:=1;
BEGIN
  WHILE i <= TEN LOOP
     INSERT INTO number_table   
     VALUES(i);
     i := i + 100;
  END LOOP;
END;
/

-- Kivételkezelés példái

-- Too many rows
DECLARE 
   num_row  number_table%ROWTYPE;
BEGIN
   SELECT * 
   INTO num_row
   FROM number_table;
   dbms_output.put_line(1/num_row.num);
EXCEPTION
   WHEN NO_DATA_FOUND THEN
	dbms_output.put_line('No data!');
   WHEN TOO_MANY_ROWS THEN
	dbms_output.put_line('Too many!');
   WHEN OTHERS THEN
	dbms_output.put_line('Error');
END;

-- No data
DECLARE 
   num_row  number_table%ROWTYPE;
BEGIN
   SELECT * 
   INTO num_row
   FROM number_table
   WHERE num < 0;
   dbms_output.put_line(1/num_row.num);
EXCEPTION
   WHEN NO_DATA_FOUND THEN
	dbms_output.put_line('No data!');
   WHEN TOO_MANY_ROWS THEN
	dbms_output.put_line('Too many!');
   WHEN OTHERS THEN
	dbms_output.put_line('Error');
END;


