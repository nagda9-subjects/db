-- 1. Feladat: Szamold meg titulus/nev elotag szerint a dolgozokat! 

SELECT
    substr(staff_name, 0, instr(staff_name, ' ')),
    COUNT(staff_id)
FROM
    stp.staff
GROUP BY
    substr(staff_name, 0, instr(staff_name, ' '));

-- 2. Feladt: Szamold meg, hogy az egyes evekben hany uj beteget vettek fel! 

SELECT
    to_char(admission_date, 'yyyy') AS "year",
    COUNT(patient_id) AS "patient num"
FROM
    stp.patient
GROUP BY
    to_char(admission_date, 'yyyy');

-- 3. Feladat: Listazd ki a betegeket felvetelukkor betoltott korcsoportos bontasban (10 eves intervallummal)!

SELECT
    ( floor(age / 10) * 10 )
    || '-'
    || ( floor(age / 10) * 10 + 10 ) AS age,
    COUNT(patient_id)
FROM
    stp.patient
GROUP BY
    ( floor(age / 10) * 10 )
    || '-'
    || ( floor(age / 10) * 10 + 10 );

--5. Feladat. Melyik dolgoz�nak mennyivel t�bb vagy kevesebb a fizet�se, mint az �tlagos fizet�s?   

SELECT
    staff_name,
    salary - (
        SELECT
            AVG(salary)
        FROM
            stp.staff
    ) AS "difference"
FROM
    stp.staff;

--6. Feladat. Ird ki azokat a kezeleseket amelyek dragabbak voltak mint a kezelesek atlagara!  

SELECT
    treatment_id
FROM
    stp.treatment
WHERE
    t_cost > (
        SELECT
            AVG(t_cost)
        FROM
            stp.treatment
    );


--7. Feladat. Mi a fonokok atlagos jovedelme?

SELECT
    AVG(salary)
FROM
    stp.staff
WHERE
    staff_id IN (
        SELECT
            manager_id
        FROM
            stp.staff
    );

SELECT
    AVG(sub.salary)
FROM
    (
        SELECT DISTINCT
            s2.staff_name,
            s2.salary
        FROM
            stp.staff   s1
            INNER JOIN stp.staff   s2 ON s2.staff_id = s1.manager_id
    ) sub;

--8. Feladat. Atlagosan, aki fonok vagy orvos mennyivel keres tobbet azoknal, akik nem tartoznak ezekbe a csoportokba (se nem fonok, se nem orvos)?

SELECT
    (
        SELECT
            AVG(salary)
        FROM
            stp.staff
        WHERE
            staff_id IN (
                SELECT
                    manager_id
                FROM
                    stp.staff
            )
            OR staff_name LIKE 'Dr%'
    ) - (
        SELECT
            AVG(salary)
        FROM
            stp.staff
        WHERE
            staff_id NOT IN (
                SELECT
                    manager_id
                FROM
                    stp.staff
                WHERE
                    manager_id IS NOT NULL
            )
            AND staff_name NOT LIKE 'Dr%'
    ) AS difference
FROM
    dual;
-- A dual helyett c�lszer�bb a FROM-ba tenni a SELECT-eket ',' -vel elv�lasztva

--9. Azok a tan�rok, akiknek nem neurobiol�gia a kutat�si ter�let�k (research_area='neurobiology�), �sszesen h�ny t�rgyat tan�tanak?

SELECT
    COUNT(s.id)
FROM
    slcd.lecturer   l
    LEFT OUTER JOIN slcd.subject    s ON l.id = s.lecturer
WHERE
    lower(l.research_area) NOT LIKE 'neurobiology';

--10. H�ny t�rgyat tan�tanak fejenk�nt azok a tan�rok, akiknek nem neurobiol�gia a kutat�si ter�let�k?

SELECT
    lect.name,
    COUNT(subj.name)
FROM
    (
        SELECT
            name,
            research_area,
            id
        FROM
            slcd.lecturer
        WHERE
            name NOT IN (
                SELECT
                    name
                FROM
                    slcd.lecturer
                WHERE
                    lower(research_area) = 'neurobiology'
            )
    ) lect
    LEFT OUTER JOIN slcd.subject subj ON lect.id = subj.lecturer
GROUP BY
    lect.name,
    lect.research_area;

--11. Hany tanar van ezek kozott, akik 2-nel kevesebb targyat tanitanak? 

SELECT
    lect.id,
    lect.name,
    lect.research_area,
    COUNT(s.id) AS num_of_subjects
FROM
    slcd.lecturer   lect
    LEFT OUTER JOIN slcd.subject    s ON lect.id = s.lecturer
WHERE
    lower(lect.research_area) NOT LIKE 'neurobiology'
GROUP BY
    lect.id,
    lect.name,
    lect.research_area
HAVING
    COUNT(s.id) < 2;

-- 12. Adjuk meg kampuszonk�nti felbont�sban a tan�rok �s a department-ek sz�m�t!

SELECT
    sub.name,
    COUNT(d.id),
    sub.lect_num
FROM
    (
        SELECT
            c.id,
            c.name,
            COUNT(l.id) AS lect_num
        FROM
            slcd.campus     c
            LEFT OUTER JOIN slcd.lecturer   l ON c.id = l.campus_id
        GROUP BY
            c.name,
            c.id
    ) sub
    LEFT OUTER JOIN slcd.department d ON sub.id = d.campus_id
GROUP BY
    sub.name,
    sub.lect_num;

-- 13. feladat Hany targyat tart az a campus, ahol Rita Lombic tanit?  

SELECT
    COUNT(s.id) AS subject_count
FROM
    (
        SELECT
            l.id,
            l.name
        FROM
            (
                SELECT
                    campus_id
                FROM
                    slcd.lecturer
                WHERE
                    name LIKE 'Rita Lombic'
            ) sub1
            LEFT OUTER JOIN slcd.lecturer l ON sub1.campus_id = l.campus_id
    ) sub2
    LEFT OUTER JOIN slcd.subject s ON sub2.id = s.lecturer;



SELECT
    *
FROM
    slcd.campus       c
    INNER JOIN slcd.department   d ON c.id = d.campus_id;

SELECT
    sub2.dep_name,
    COUNT(s.id)
FROM
    (
        SELECT
            sub1.campus_id,
            dep_name,
            l.id AS lec_id
        FROM
            (
                SELECT
                    c.id     AS campus_id,
                    d.name   AS dep_name
                FROM
                    slcd.campus       c
                    INNER JOIN slcd.department   d ON c.id = d.campus_id
            ) sub1
            INNER JOIN slcd.lecturer l ON sub1.campus_id = l.campus_id
    ) sub2
    INNER JOIN slcd.subject s ON sub2.lec_id = s.lecturer
GROUP BY
    sub2.dep_name;

-- 14. Melyik departmenten hany targyat tartanak?

SELECT
    sub1.campus_name,
    COUNT(l.id)
FROM
    (
        SELECT
            c.id     AS campus_id,
            c.name   AS campus_name,
            COUNT(d.id) AS dep_num
        FROM
            ( slcd.campus       c
            LEFT OUTER JOIN slcd.department   d ON c.id = d.campus_id )
        GROUP BY
            c.id,
            c.name
    ) sub1
    INNER JOIN slcd.lecturer l ON sub1.campus_id = l.campus_id
WHERE
    lower(l.research_area) NOT LIKE 'neurobiology'
GROUP BY
    sub1.campus_name;

--16. Dekomponald a tablat ket kulon tablara (persons, buildings). A kozos attributum a building legyen!
--veszteseges dekompozicio; kozos attributum building   

DROP TABLE persons;
DROP TABLE buildings;

CREATE TABLE persons
    AS
        SELECT DISTINCT
            id,
            name,
            salary,
            building
        FROM
            allinfo;

CREATE TABLE buildings
    AS
        SELECT DISTINCT
            building,
            dept_name,
            budget
        FROM
            allinfo;

--17. Ellenorizd le, hogy a ket keletkezett tabla JOIN-ja (inner join) ugyanazokat a sorokat tartalmazza-e, mint az eredeti tabla!   
--a ketto joinja; --> sortobbszorozodes; veszteseges decomp.   

SELECT
    *
FROM
    persons     p
    INNER JOIN buildings   b ON p.building = b.building;
-- t�bb sort tartalmaz -> nem ugyan azokat a sorokat tartalmazza

-- Egy m�sik m�dszer, kivonjuk a joint az eredeti t�bl�b�l, �s megn�zz�k marad-e adat:
SELECT id, 
       name, 
       salary, 
       dept_name, 
       buildings.building, 
       budget 
FROM   persons 
       inner join buildings 
               ON persons.building = buildings.building 
MINUS 
SELECT * 
FROM   allinfo; 

--18. Vizsg�ld meg az adatok jelenlegi el�fordul�sa kiel�g�ti-e, a building -> {dept_name,budget} funkcion�lis f�gg�s�get!

SELECT
    building,
    COUNT(dept_name)
FROM
    buildings
GROUP BY
    building
HAVING
    COUNT(dept_name) > 1;
-- mivel van olyan hogy egy buildinghez tobb kulonbozo department/budget tartozik, 
--  ez a funkc fugges ezeken az adatokon nem �ll fenn

SELECT
    building,
    COUNT(budget)
FROM
    buildings
GROUP BY
    building
HAVING
    COUNT(budget) > 1;

--19. Dekomponald a tablat ket kulon tablara (persons2, departments). A kozos attributum a dept_name legyen!
--vesztesegmentes; kozos attributum dept_name   

CREATE TABLE persons2
    AS
        SELECT DISTINCT
            id,
            name,
            salary,
            dept_name
        FROM
            allinfo;

CREATE TABLE departments
    AS
        SELECT DISTINCT
            dept_name,
            building,
            budget
        FROM
            allinfo;

--20. Ellenorizd le, hogy a ket keletkezett tabla JOIN-ja ugyanazokat a sorokat tartalmazza-e, mint az eredeti tabla!

SELECT
    *
FROM
    persons2      p
    INNER JOIN departments   d ON p.dept_name = d.dept_name;

SELECT id, 
       name, 
       salary, 
       departments.dept_name, 
       building, 
       budget 
FROM   persons2 
       inner join departments 
               ON persons2.dept_name = departments.dept_name 
MINUS 
SELECT * 
FROM   allinfo; 

--21. Vizsg�ld meg az adatok jelenlegi el�fordul�sa kiel�g�ti-e, a dept_name -> {budget,building} funkcion�lis f�gg�s�get!

SELECT
    dept_name,
    COUNT(budget)
FROM
    departments
GROUP BY
    dept_name
HAVING
    COUNT(budget) > 1;
-- mivel nincs olyan hogy egy dept_namehez tobb kulonbozo building/budget tartozik, 
--  ezeken az adatokon fenn�ll a funkc fugges

SELECT
    dept_name,
    COUNT(building)
FROM
    departments
GROUP BY
    dept_name
HAVING
    COUNT(building) > 1;
-- teljes�l a funkcion�lis f�ggetlens�g