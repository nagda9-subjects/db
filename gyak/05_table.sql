drop table pubs cascade constraints purge;
drop table people cascade constraints purge;
drop table towns cascade constraints purge;
drop table guests cascade constraints purge;


CREATE TABLE towns(
	postal_code NUMBER PRIMARY KEY,
	town_name VARCHAR(255) NOT NULL,
	population NUMBER,
	area NUMBER
);

CREATE TABLE pubs(
	pub_id NUMBER GENERATED ALWAYS AS IDENTITY,
	pub_name VARCHAR(255) NOT NULL,
	capacity NUMBER,
	foundation DATE,
    town_postal_code NUMBER NOT NULL,
    CONSTRAINT pub_pk PRIMARY KEY (pub_id),
    CONSTRAINT town_fk FOREIGN KEY (town_postal_code) REFERENCES towns (postal_code)
);

CREATE TABLE people(
	id NUMBER GENERATED ALWAYS AS IDENTITY,
	first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
	sex VARCHAR(6),
	date_of_birth DATE,
    postal_code NUMBER NOT NULL,
    address VARCHAR2(40),
    job VARCHAR2(40),
	CONSTRAINT person_pk PRIMARY KEY (id),
    CONSTRAINT hometown_fk FOREIGN KEY (postal_code) REFERENCES towns (postal_code)
);

CREATE TABLE guests (
    guest_id NUMBER GENERATED ALWAYS AS IDENTITY,
    person_id NUMBER NOT NULL,
    pub_id NUMBER NOT NULL,
    date_of_visit DATE NOT NULL,
    CONSTRAINT guest_pk PRIMARY KEY (guest_id),
    CONSTRAINT person_fk FOREIGN KEY (person_id) REFERENCES people (id),
    CONSTRAINT pub_fk FOREIGN KEY (pub_id) REFERENCES pubs (pub_id)
);

INSERT INTO towns VALUES(3120, 'Boldogságfalva', 3231, 83.75);
INSERT INTO towns VALUES(1211, 'Búbánatvölgye', 211, 63.59);
INSERT INTO towns VALUES(1000, 'MetroCity', 423231, 513.23);
INSERT INTO towns VALUES(6464, 'OldTown', 731, 34.14);

INSERT INTO pubs(pub_name, capacity, foundation, town_postal_code) 
VALUES ('Fogadó a rózsaszín szivárványhoz', 100, to_date('1943-02-13', 'yyyy-mm-dd'), 3120);
INSERT INTO pubs(pub_name, capacity, foundation, town_postal_code)
VALUES ('Happy Pub', 200, to_date('1993-08-05', 'yyyy-mm-dd'), 3120);
INSERT INTO pubs(pub_name, capacity, foundation, town_postal_code)
VALUES ('Italda', 200, to_date('1981-05-10', 'yyyy-mm-dd'), 3120);

INSERT INTO pubs(pub_name, capacity, foundation, town_postal_code)
VALUES ('Fogadó a járókerethez', 50, to_date('1923-08-05', 'yyyy-mm-dd'), 6464);
INSERT INTO pubs(pub_name, capacity, foundation, town_postal_code)
VALUES ('Ősz szarvas', 150, to_date('1901-02-23', 'yyyy-mm-dd'), 6464);

INSERT INTO pubs(pub_name, capacity, foundation, town_postal_code)
VALUES ('Metró Pub', 250, to_date('1997-10-03', 'yyyy-mm-dd'), 1000);
INSERT INTO pubs(pub_name, capacity, foundation, town_postal_code)
VALUES ('A 7 Mesterlövész', 777, to_date('1977-07-07', 'yyyy-mm-dd'), 1000);
INSERT INTO pubs(pub_name, capacity, foundation, town_postal_code)
VALUES ('Andersen 4', 230, to_date('2010-11-16', 'yyyy-mm-dd'), 1000);
INSERT INTO pubs(pub_name, capacity, foundation, town_postal_code)
VALUES ('Morrisons 8', 1210, to_date('2009-04-05', 'yyyy-mm-dd'), 1000);
INSERT INTO pubs(pub_name, capacity, foundation, town_postal_code)
VALUES ('Pub G', 195, to_date('2015-03-09', 'yyyy-mm-dd'), 1000);
INSERT INTO pubs(pub_name, capacity, foundation, town_postal_code)
VALUES ('Harmat Söröző', 130, to_date('1999-10-27', 'yyyy-mm-dd'), 1000);


INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Kerek', 'Ferenc', to_date('1957-11-23','yyyy-mm-dd'), 'male', 'engineer', 6464, 'Pipacs utca 52');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Kováts', 'Izidor', to_date('1954-01-30','yyyy-mm-dd'), 'male', 'priest', 6464, 'Kereszt utca 7');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Nagy', 'Elemér', to_date('1942-09-10','yyyy-mm-dd'), 'male', 'teacher', 6464, 'Kossúth Lajos utca 22');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Dr. Korosh', 'Judith', to_date('1939-04-22','yyyy-mm-dd'), 'female', 'doctor', 6464, 'Fő út 22');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Dr. Vén', 'Klotild', to_date('1921-09-13','yyyy-mm-dd'), 'female', 'doctor', 6464, 'Megboldog út 13');


INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Miezitt', 'Emily', to_date('1989-04-22','yyyy-mm-dd'), 'female', 'engineer', 3120, 'Mellék út 12');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Dr. Ügyesvagy', 'Klotild', to_date('1984-04-22','yyyy-mm-dd'), 'female', 'teacher', 3120, 'Boldog út 12');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Öröm', 'Ernő', to_date('1997-04-22','yyyy-mm-dd'), 'male', 'student', 3120, 'Fő út 5');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Öröm', 'Emese', to_date('1999-09-12','yyyy-mm-dd'), 'female', 'student', 3120, 'Fő út 5');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Felüdült', 'Jácint', to_date('2001-09-12','yyyy-mm-dd'), 'female', 'student', 3120, 'Happy utca 5');


INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Savanyú', 'Elemér', to_date('1964-06-13','yyyy-mm-dd'), 'male', 'engineer', 1211, 'Bú út 13');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Dr. Szomorú', 'Béla', to_date('1981-09-13','yyyy-mm-dd'), 'male', 'doctor', 1211, 'Könny utca 6');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Emós', 'Pál', to_date('1998-07-13','yyyy-mm-dd'), 'male', 'student', 1211, 'Szomor út 31');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Életunt', 'Emőke', to_date('1987-03-13','yyyy-mm-dd'), 'female', 'shop assistant', 1211, 'Bú út 13');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Mindenbaj', 'Evelin', to_date('1998-07-13','yyyy-mm-dd'), 'female', 'student', 1211, 'Könny utca 11');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Bánatos', 'Botond', to_date('1978-08-17','yyyy-mm-dd'), 'male', 'engineer', 1211, 'Könny utca 13');


INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Nagy', 'Balázs', to_date('2003-05-02','yyyy-mm-dd'), 'male', 'student', 1000, 'Fő út 10');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Dr. Ügyvéd', 'László', to_date('1965-05-02','yyyy-mm-dd'), 'male', 'lawyer', 1000, 'Felis út 15');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Dr. Igaz', 'Etelka', to_date('1965-05-02','yyyy-mm-dd'), 'female', 'lawyer', 1000, 'Leis út 11');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Kiss', 'Enikő', to_date('2002-07-27','yyyy-mm-dd'), 'female', 'student', 1000, 'Galamb utca 10');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Mega', 'Agy', to_date('1983-05-02','yyyy-mm-dd'), 'male', 'evil mastermind', 1000, 'Fortély utca 17');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Jóbarát', 'Pál', to_date('1999-05-05','yyyy-mm-dd'), 'male', 'student', 1000, 'Fő út 31251');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Krajczár', 'Ivett', to_date('2001-04-09','yyyy-mm-dd'), 'female', 'student', 1000, 'Kis utca 121');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Dr. Kelemen', 'Anikó', to_date('1973-01-19','yyyy-mm-dd'), 'female', 'doctor', 1000, 'Fő út 31251');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Újítás', 'Paula', to_date('1981-06-13','yyyy-mm-dd'), 'female', 'engineer', 1000, 'Fő út 11111');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Híd', 'Pál', to_date('1984-05-03','yyyy-mm-dd'), 'male', 'engineer', 1000, 'Fő út 12010');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Haver', 'Rebeka', to_date('1999-03-12','yyyy-mm-dd'), 'female', 'student', 1000, 'Napsugár utca 21');
INSERT INTO people(last_name, first_name, date_of_birth, sex, job, postal_code, address)
VALUES('Dr. Jóbarát', 'Ernő', to_date('1971-03-17','yyyy-mm-dd'), 'male', 'lawyer', 1000, 'Fő út 31251');

INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (6, 1, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (7, 1, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (6, 1, to_date('2020-01-22', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (9, 1, to_date('2020-01-22', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (7, 1, to_date('2020-01-22', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (9, 1, to_date('2020-01-23', 'yyyy-mm-dd'));

INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (8, 2, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (10, 2, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (11, 2, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (12, 2, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (5, 2, to_date('2020-01-22', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (8, 2, to_date('2020-01-22', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (10, 2, to_date('2020-01-23', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (6, 2, to_date('2020-01-23', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (7, 2, to_date('2020-01-23', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (8, 2, to_date('2020-01-23', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (1, 2, to_date('2020-01-23', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (8, 2, to_date('2020-01-24', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (9, 2, to_date('2020-01-24', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (10, 2, to_date('2020-01-24', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (7, 2, to_date('2020-01-24', 'yyyy-mm-dd'));

INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (3, 3, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (9, 3, to_date('2020-01-21', 'yyyy-mm-dd'));

INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (1, 4, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (2, 4, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (5, 4, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (4, 4, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (3, 4, to_date('2020-01-22', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (5, 4, to_date('2020-01-22', 'yyyy-mm-dd'));

INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (1, 5, to_date('2020-01-24', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (2, 5, to_date('2020-01-24', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (3, 5, to_date('2020-01-24', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (4, 5, to_date('2020-01-24', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (5, 5, to_date('2020-01-24', 'yyyy-mm-dd'));

INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (7, 7, to_date('2020-01-22', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (17, 7, to_date('2020-01-22', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (19, 7, to_date('2020-01-23', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (22, 7, to_date('2020-01-23', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (25, 7, to_date('2020-01-24', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (17, 7, to_date('2020-01-24', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (21, 7, to_date('2020-01-24', 'yyyy-mm-dd'));


INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (24, 8, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (25, 8, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (16, 8, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (17, 8, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (18, 8, to_date('2020-01-24', 'yyyy-mm-dd'));


INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (18, 9, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (17, 9, to_date('2020-01-23', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (20, 9, to_date('2020-01-23', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (20, 9, to_date('2020-01-24', 'yyyy-mm-dd'));

INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (12, 11, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (13, 11, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (18, 11, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (19, 11, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (20, 11, to_date('2020-01-21', 'yyyy-mm-dd'));
INSERT INTO guests (person_id, pub_id, date_of_visit) VALUES (22, 11, to_date('2020-01-22', 'yyyy-mm-dd'));

COMMIT;

