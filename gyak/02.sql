--0.1 Feladat: Hozd létre a következő táblát, és töltsd fel az alábbi értékekkel! Állítsd be a megfelelő attribútumot primary key-nek! 

DROP TABLE fruit;

CREATE TABLE fruit (
    fruit_id         CHAR(5),
    f_name           VARCHAR(30),
    color            VARCHAR(30),
    best_before      DATE,
    amount_in_tons   NUMBER
);

ALTER TABLE fruit ADD CONSTRAINT pk_friut PRIMARY KEY ( fruit_id );

ALTER TABLE fruit MODIFY
    f_name VARCHAR(30) NOT NULL;

ALTER TABLE fruit MODIFY
    color VARCHAR(20) NOT NULL;

INSERT INTO fruit (
    fruit_id,
    f_name,
    color,
    best_before,
    amount_in_tons
) VALUES (
    'F1500',
    'apple',
    'red',
    TO_DATE('24-10-2004', 'dd-mm-yyyy'),
    21
);

INSERT INTO fruit (
    fruit_id,
    f_name,
    color,
    best_before,
    amount_in_tons
) VALUES (
    'F9700',
    'peach',
    'peach',
    TO_DATE('02-08-2010', 'dd-mm-yyyy'),
    5
);

INSERT INTO fruit (
    fruit_id,
    f_name,
    color,
    best_before,
    amount_in_tons
) VALUES (
    'F9500',
    'apple',
    'green',
    TO_DATE('04-09-2010', 'dd-mm-yyyy'),
    32
);

COMMIT;

SELECT
    *
FROM
    fruit;

-- 0.2 Feladat. Válaszd ki sorszámozva (rownum) azon gyümölcsök azonosítóját és mennyiségét, ahol a mennyiség nagyobb, mint 10 tonna!

SELECT
    ROWNUM,
    fruit_id,
    amount_in_tons
FROM
    fruit
WHERE
    amount_in_tons > 10;

-- 0.3 Feladat. Válaszd ki sorszámozva azon gyümölcsök nevét és lejárati dátumát, amelyek 2006 után még fogyaszthatók! A kiíratás legyen mennyiség szerint növekvő sorrendbe rendezve

SELECT
    ROWNUM,
    f_name,
    best_before
FROM
    fruit
WHERE
    best_before >= TO_DATE('2006-01-01', 'yyyy-mm-dd')
ORDER BY
    amount_in_tons ASC;

-- 0.4 Feladat. Írasd ki, milyen gyümölcsök vannak az adatbázisban (név szerint). Egy gyümölcsöt csak egyszer írj ki, akkor is ha többször szerepel a táblában! (DISTINCT)

SELECT DISTINCT
    f_name
FROM
    fruit;

--1. Feladat. Melyik kezelest ki vegezte es mennyibe kerult? Az eredmenyt rendezd kezeles ara alapjan csokkeno sorrendbe!

SELECT
    t.treatment_id,
    s.staff_name,
    t.t_cost
FROM
    stp.treatment   t
    INNER JOIN stp.staff       s ON t.consultant = s.staff_id
ORDER BY
    t.t_cost DESC;

-- ugyanaz csak view-val:

CREATE OR REPLACE VIEW feladat_gyak2_1 AS
    SELECT
        t.treatment_id,
        s.staff_name,
        t.t_cost
    FROM
        stp.treatment   t
        INNER JOIN stp.staff       s ON t.consultant = s.staff_id
    ORDER BY
        t.t_cost DESC;

-- 2. Feladat Modositsd ugy az elozo lekerdezest, hogy azokat is kiirja, akik nem vegeztek kezelest! (outer join) 

SELECT
    t.treatment_id,
    s.staff_name,
    t.t_cost
FROM
    stp.treatment   t
    RIGHT OUTER JOIN stp.staff       s ON t.consultant = s.staff_id
ORDER BY
    t.t_cost DESC;

-- 3. Feladat. Melyik beteget ki kezelte es mikor?

SELECT
    p.patient_name,
    s.staff_name,
    t.t_time
FROM
    ( stp.staff       s
    INNER JOIN stp.treatment   t ON s.staff_id = t.consultant )
    INNER JOIN stp.patient     p ON t.patient_id = p.patient_id;

-- 4. Feladat. Kinek ki a kozvetlen fonoke?

SELECT
    t1.staff_name   AS beosztott,
    t2.staff_name   AS fonok
FROM
    stp.staff   t1
    INNER JOIN stp.staff   t2 ON t1.manager_id = t2.staff_id;

-- 5. Feladat. Ki az, aki nem vegzett egyetlen kezelest sem?

SELECT
    staff_id
FROM
    stp.staff
MINUS
SELECT
    consultant
FROM
    stp.treatment;

-- 6. Feladat. Ki az, aki fonoke valakinek, de nem o az igazgato?

SELECT
    manager_id
FROM
    stp.staff
WHERE
    manager_id IS NOT NULL
MINUS
SELECT
    staff_id
FROM
    stp.staff
WHERE
    manager_id IS NULL;

-- 7. Feladat. Listazd ki az osszes korhazban levo ember nevet, es azt, hogy beteg-e, vagy alkalmazott!

SELECT
    patient_name AS name,
    'beteg' AS status
FROM
    stp.patient
UNION
SELECT
    staff_name AS name,
    'alkalmazott' AS status
FROM
    stp.staff;

-- 8. Feladat. Ird ki a gyogyszerek nevet kisbetuvel!

SELECT DISTINCT
    lower(drug) AS drug
FROM
    stp.treatment
WHERE
    drug IS NOT NULL;