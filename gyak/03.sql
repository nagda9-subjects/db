--1. Feladat. P9500 azonositoju beteg diagnozisa megvaltozott moderate-rol severe-re. Ennek megfeleloen frissitsd az adatbazist!

SELECT
    patient_name,
    alzheimer_diagnosis
FROM
    patient
WHERE
    patient_id = 'P9500';

-- (ellen�rz� lek�rdez�s)

UPDATE patient
SET
    alzheimer_diagnosis = 'severe'
WHERE
    patient_id = 'P9500';

--2. Feladat. Frissitsd az eletkort ugy, hogy az aktualis eletkort mutassa, ne pedig a felvetelkorit!

UPDATE patient
SET
    age = age + round((sysdate - admission_date) / 365);

SELECT
    patient_name,
    age
FROM
    patient;

--3. Feladat. Torold az osszes nonemu beteget!

DELETE FROM patient
WHERE
    sex = 'female';
-- ezt engedi probl�ma n�lk�l, mivel az egyetlen n�nek nincs semmilyen adata a treatment t�bl�ban

--4. Feladat. A Henry Kip nevu beteget athelyeztek, torold az adatbazisbol!
-- Hib�t adna a FK miatt, ez�rt constraint elt�vol�t�sa.

SELECT
    *
FROM
    patient     p
    INNER JOIN treatment   t ON p.patient_id = t.patient_id;

ALTER TABLE treatment DROP CONSTRAINT patient_fk;

ALTER TABLE treatment
    ADD CONSTRAINT patient_fk FOREIGN KEY ( patient_id )
        REFERENCES patient ( patient_id )
            ON DELETE CASCADE; 

DELETE FROM patient
WHERE
    patient_name = 'Henry Kip';

--5. Feladat. Torold az osszes rekordot a tablabol!

DELETE FROM patient;

-- Extra takar�t�s:

ALTER TABLE treatment DROP CONSTRAINT patient_fk;

DROP TABLE patient;

ALTER TABLE treatment DROP CONSTRAINT consultant_fk;

DROP TABLE treatment;

DROP TABLE staff;

COMMIT;


--6. Feladat. Mennyi az atlag fizetes?

SELECT
    AVG(salary) AS "atlag_fizetes"
FROM
    stp.staff;

--7. Feladat. Egy honapban a korhaz mennyi penzt kolt a dolgozok fizetesere?

SELECT
    SUM(salary) AS "havi fizetes"
FROM
    stp.staff;


--8. Feladat. Mikor volt a legkorabbi es a legkesobbi kezeles?

SELECT
    MIN(t_time) AS "legkorabbi",
    MAX(t_time) AS "legkesobbi"
FROM
    stp.treatment;


--9. Feladat. Hany kezeles volt osszesen?

SELECT
    COUNT(treatment_id) AS "kezelesek szama"
FROM
    stp.treatment;

--10. Feladat. Hanyszor adtak gyogyszert osszesen?

SELECT
    COUNT(drug) AS "gyogyszeres kezeles"
FROM
    stp.treatment;

--11. Feladat. Opcion�lis: mi a betegek vezet�k - es keresztneve? Milyen fuggvenyek kellenek ehhez? (hasznald a google-t)

SELECT
    patient_name,
    substr(patient_name, 0, instr(patient_name, ' ')) AS "first name",
    substr(patient_name, instr(patient_name, ' ') + 1) AS "last name"
FROM
    stp.patient;

--FIGYELNI A KETTOS ES SZIMPLA IDEZOJELEKRE
--substr(szoveg amiben keresunk, szovegresz eleje, [szovegresz vege]) -> r�sszoveg, ami tartalmazza a szoveg elejet, de a veget nem
--instr(szoveg amiben keresunk, keresett szoveg) -> karakter indexe


--�NOT A GROUP BY EXPRESSION� -- p�ld�k

/*0.1 Szerett�k volna ki�ratni, a macsk�k sz�m�t a gazd�jukkal.

Az eredeti lek�rdez�s:
SELECT p.id, 
       p.name, 
       Count(c.id) AS cat_count 
FROM   cpd.person p 
       LEFT OUTER JOIN cpd.cat c 
                    ON p.id = c.person_id 
GROUP  BY p.id;

Itt csak annyi t�rt�nt, hogy kimaradt a p.name a GROUP BY-b�l.*/

-- A hiba orvosolhat�, ha kihagyjuk a p.name attrib�tumot a lek�rdez�sbol. 
SELECT p.id, 
       Count(c.id) AS cat_count 
FROM   cpd.person p 
       LEFT OUTER JOIN cpd.cat c 
                    ON p.id = c.person_id 
GROUP  BY p.id; 

-- Ha a nevet is szeretn�nk, akkor ezt is betessz�k a group by-ba (az sqldeveloper is tudja nek�nk jav�tani a tooltip seg�ts�g�vel). 
SELECT p.id, 
       p.name, 
       Count(c.id) AS cat_count 
FROM   cpd.person p 
       LEFT OUTER JOIN cpd.cat c 
                    ON p.id = c.person_id 
GROUP  BY p.id, p.name; 

/*0.2 Szerett�k volna ki�ratni, a macsk�k sz�m�t �s nev�ket is, a gazd�juk nev�vel.

Az eredeti lek�rdez�s:
SELECT p.name, 
       Count(c.id) AS cat_count,
       c.name
FROM   cpd.person p 
       LEFT OUTER JOIN cpd.cat c 
                    ON p.id = c.person_id 
GROUP  BY p.id,p.name;

Mivel van aggreg�l�s, egy-egy ember mell� lehet t�bb macska is, ez okozza a hib�t.*/

-- A macsk�k nev�t egy �rt�kbe aggreg�lhatjuk a LISTAGG f�ggv�ny seg�ts�g�vel (stringek eset�n lehet j� megold�s).
SELECT p.name, 
       Count(c.id)                     AS cat_count, 
       Listagg(c.name, ', ') 
         WITHIN GROUP (ORDER BY(c.id)) AS cat_name_list 
FROM   cpd.person p 
       LEFT OUTER JOIN cpd.cat c 
                    ON p.id = c.person_id 
GROUP  BY p.id, p.name;

--12. Feladat �rasd ki a betegek �tlag�letkor�t nemenk�nt!

SELECT
    sex,
    AVG(age)
FROM
    stp.patient
GROUP BY
    sex;

--13 �rasd ki az azonos betuvel kezdodo nevu emberek �tlag�letkor�t!

SELECT
    AVG(age)
FROM
    stp.patient
GROUP BY 
    sex
HAVING
    sex = 'male';

SELECT
    substr(patient_name, 0, 1),
    AVG(age)
FROM
    stp.patient
GROUP BY
    substr(patient_name, 0, 1);

--14 Feladat. Az egyes gy�gyszereket h�nyszor haszn�lt�k kezel�sre?

SELECT
    drug,
    COUNT(drug)
FROM
    stp.treatment
GROUP BY
    drug
HAVING
    drug IS NOT NULL;

SELECT
    drug,
    COUNT(*)
FROM
    stp.treatment
GROUP BY
    drug;

--15. Feladat. Csak azokat a gyogyszereket listazd ki, amelyeket legalabb ketszer hasznaltak, a kezel�sek �sszk�lts�g�g�vel egy�tt!

SELECT
    drug,
    SUM(t_cost)
FROM
    stp.treatment
GROUP BY
    drug
HAVING
    COUNT(drug) >= 2;

SELECT
    drug,
    COUNT(*),
    SUM(t_cost)
FROM
    stp.treatment
GROUP BY
    drug
HAVING
    COUNT(*) >= 2;

--16. Feladat. List�zd ki betegenk�nt a kezel�sek sz�m�t! (id, n�v, kezel�sek sz�ma)

SELECT
    p.patient_id,
    p.patient_name,
    COUNT(t.treatment_id)
FROM
    stp.patient     p
    LEFT OUTER JOIN stp.treatment   t ON t.patient_id = p.patient_id
GROUP BY
    p.patient_id,
    p.patient_name;

--17. Feladat. Melyik orvos h�ny kezel�st v�gzett el? (Csak azokat list�zd, akik legal�bb 1 kezel�st v�geztek el)

SELECT
    s.staff_name,
    COUNT(t.treatment_id)
FROM
    stp.staff       s
    INNER JOIN stp.treatment   t ON t.consultant = s.staff_id
GROUP BY
    s.staff_name;
--nem kell semmi felt�tel, mivel az inner join kisz�ri aki nem v�gzett kezel�st

--18. Feladat. Az egyes beoszt�sokban (staff.post) mik az �tlagos j�vedelmek? 

SELECT
    s.post,
    AVG(s.salary)
FROM
    stp.staff s
GROUP BY
    s.post;