--select * from mb18___db.historyitems;
--create or replace view history as select * from mb18___db.historyitems;
--drop view history;
--drop procedure nagdaz_fz1au9;

--select * from all_users;
--select username from all_users where lower(username) like lower('MB18___db');
--select * from all_tables;
--select table_name from all_tables where lower(owner) like 'mb18___db' and lower(table_name) like 'historyitems';
--select * from all_tab_columns;
--select column_name from all_tab_columns where lower(owner) like lower('mb18___db') and lower(table_name) like lower('historyitems') and lower(data_type) like 'number';

--select count(user_id),
--    to_char(avg(user_id), '9.90EEEE'), 
--    to_char(stddev(user_id), '9.90EEEE'), 
--    to_char(min(user_id), '9.90EEEE'),
--    to_char(PERCENTILE_DISC(0.25) within group (order by user_id), '9.90EEEE'),
--    to_char(PERCENTILE_DISC(0.5) within group (order by user_id), '9.90EEEE'),
--    to_char(PERCENTILE_DISC(0.75) within group (order by user_id), '9.90EEEE'),
--    to_char(max(user_id), '9.90EEEE') 
--    from mb18___db.historyitems;

--select max(length(column_name))
--from all_tab_columns 
--where lower(owner) like lower('mb18___db') and lower(table_name) like lower('historyitems') and lower(data_type) like 'number';

set serveroutput on;

create or replace procedure mb20_nagdaz_fz1au9(
    usr in varchar2,
    tbl in varchar2)
AS
    no_existing_schema exception;
    no_existing_table exception;
    no_number_attr exception;
    
    cursor c_usr is select username from all_users where lower(username) like lower(usr);
    current_usr c_usr%rowtype;
    
    cursor c_tbl is select table_name from all_tables where lower(owner) like lower(usr) and lower(table_name) like lower(tbl);
    current_tbl c_tbl%rowtype;
    
    cursor c_attr is select column_name from all_tab_columns where lower(owner) like lower(usr) and lower(table_name) like lower(tbl) and lower(data_type) like 'number';
    current_attr c_attr%rowtype;
    
    type c_data_type is ref cursor;
    c_data c_data_type;
    
    TYPE data_row_type is record(
        d_count varchar2(100),
        d_mean varchar2(100),
        d_std varchar2(100),
        d_min varchar2(100),
        d_q1 varchar2(100),
        d_q2 varchar2(100),
        d_q3 varchar2(100),
        d_max varchar2(100)
    );  
    current_data data_row_type;
    
    fcw number := 0;
    cw constant number := 20;
begin 
    open c_usr;
    fetch c_usr into current_usr;
    if c_usr%notfound then
        close c_usr;
        raise no_existing_schema;
    else
        open c_tbl;
        fetch c_tbl into current_tbl;
        if c_tbl%notfound then
            close c_tbl;
            raise no_existing_table;
        else
            select max(length(column_name)) + length(tbl) +1 into fcw
            from all_tab_columns 
            where lower(owner) like lower(usr) and lower(table_name) like lower(tbl) and lower(data_type) like 'number';

            dbms_output.put_line('');
            dbms_output.put_line(
                         lpad('TABLE.COLUMN', fcw)
                         || lpad('COUNT', cw)
                         || lpad('MEAN', cw)
                         || lpad('STD', cw)
                         || lpad('MIN', cw)
                         || lpad('P25', cw)
                         || lpad('P50', cw)
                         || lpad('P75', cw)
                         || lpad('MAX', cw));
            open c_attr;
            fetch c_attr into current_attr;
            if c_attr%notfound then
                close c_attr;
                raise no_number_attr;
            else
                close c_attr;
                open c_attr;           
                loop 
                    fetch c_attr into current_attr;
                    exit when c_attr%notfound;

                    open c_data for 'select count(' || current_attr.column_name || '),
                        to_char(avg(' || current_attr.column_name || '), ''9.90EEEE''), 
                        to_char(stddev(' || current_attr.column_name || '), ''9.90EEEE''), 
                        to_char(min(' || current_attr.column_name || '), ''9.90EEEE''),
                        to_char(PERCENTILE_DISC(0.25) within group (order by ' || current_attr.column_name || '), ''9.90EEEE''),
                        to_char(PERCENTILE_DISC(0.5) within group (order by ' || current_attr.column_name || '), ''9.90EEEE''),
                        to_char(PERCENTILE_DISC(0.75) within group (order by ' || current_attr.column_name || '), ''9.90EEEE''),
                        to_char(max(' || current_attr.column_name || '), ''9.90EEEE'') 
                        from ' || usr || '.' || tbl;
                        
                    fetch c_data INTO current_data;
                    dbms_output.put_line(
                             lpad(lower(tbl || '.' || current_attr.column_name), fcw)
                             || lpad(current_data.d_count, cw)
                             || lpad(current_data.d_mean, cw)
                             || lpad(current_data.d_std, cw)
                             || lpad(current_data.d_min, cw)
                             || lpad(current_data.d_q1, cw)
                             || lpad(current_data.d_q2, cw)
                             || lpad(current_data.d_q3, cw)
                             || lpad(current_data.d_max, cw));
                    
                end loop;
                close c_data;
                close c_attr;
            end if;    
        end if;
    end if;
exception
    WHEN no_existing_schema THEN DBMS_OUTPUT.PUT_LINE ('Schema does not exist.');
    WHEN no_existing_table THEN DBMS_OUTPUT.PUT_LINE ('Table does not exist.');
    WHEN no_number_attr THEN DBMS_OUTPUT.PUT_LINE ('Table with no number attributes.');
end;
/

execute mb20_nagdaz_fz1au9('mb18___db', 'historyitems');