--select * from mb18___db.historyitems;
--create or replace view history as select * from mb18___db.historyitems;
--drop view history;
--drop procedure nagdaz_fz1au9;

--select * from all_users;
--select username from all_users where lower(username) like lower('MB18___db');
--select * from all_tables;
--select table_name from all_tables where lower(owner) like 'mb18___db' and lower(table_name) like 'historyitems';
--select * from all_tab_columns;
--select column_name from all_tab_columns where lower(owner) like lower('mb18___db') and lower(table_name) like lower('historyitems') and lower(data_type) like 'number';

--select count(user_id),
--    to_char(avg(user_id), '9.90EEEE'), 
--    to_char(stddev(user_id), '9.90EEEE'), 
--    to_char(min(user_id), '9.90EEEE'),
--    to_char(PERCENTILE_DISC(0.25) within group (order by user_id), '9.90EEEE'),
--    to_char(PERCENTILE_DISC(0.5) within group (order by user_id), '9.90EEEE'),
--    to_char(PERCENTILE_DISC(0.75) within group (order by user_id), '9.90EEEE'),
--    to_char(max(user_id), '9.90EEEE') 
--    from mb18___db.historyitems;

--select max(length(column_name))
--from all_tab_columns 
--where lower(owner) like lower('mb18___db') and lower(table_name) like lower('historyitems') and lower(data_type) like 'number';
SET SERVEROUTPUT ON;

CREATE OR REPLACE PROCEDURE mb20_nagdaz_fz1au9 (
    usr   IN   VARCHAR2,
    tbl   IN   VARCHAR2
) AS

    no_existing_schema EXCEPTION;
    no_existing_table EXCEPTION;
    no_number_attr EXCEPTION;
    CURSOR c_usr IS
    SELECT
        username
    FROM
        all_users
    WHERE
        lower(username) LIKE lower(usr);

    current_usr    c_usr%rowtype;
    CURSOR c_tbl IS
    SELECT
        table_name
    FROM
        all_tables
    WHERE
        lower(owner) LIKE lower(usr)
        AND lower(table_name) LIKE lower(tbl);

    current_tbl    c_tbl%rowtype;
    CURSOR c_attr IS
    SELECT
        column_name
    FROM
        all_tab_columns
    WHERE
        lower(owner) LIKE lower(usr)
        AND lower(table_name) LIKE lower(tbl)
        AND lower(data_type) LIKE 'number';

    current_attr   c_attr%rowtype;
    TYPE c_data_type IS REF CURSOR;
    c_data         c_data_type;
    TYPE data_row_type IS RECORD (
        d_count   VARCHAR2(100),
        d_mean    VARCHAR2(100),
        d_std     VARCHAR2(100),
        d_min     VARCHAR2(100),
        d_q1      VARCHAR2(100),
        d_q2      VARCHAR2(100),
        d_q3      VARCHAR2(100),
        d_max     VARCHAR2(100)
    );
    current_data   data_row_type;
    fcw            NUMBER;
    cw             CONSTANT NUMBER := 20;
BEGIN
    OPEN c_usr;
    FETCH c_usr INTO current_usr;
    IF c_usr%notfound THEN
        CLOSE c_usr;
        RAISE no_existing_schema;
    ELSE
        OPEN c_tbl;
        FETCH c_tbl INTO current_tbl;
        IF c_tbl%notfound THEN
            CLOSE c_tbl;
            RAISE no_existing_table;
        ELSE
            SELECT
                MAX(length(column_name)) + length(tbl) + 1
            INTO fcw
            FROM
                all_tab_columns
            WHERE
                lower(owner) LIKE lower(usr)
                AND lower(table_name) LIKE lower(tbl)
                AND lower(data_type) LIKE 'number';

            dbms_output.put_line('');
            dbms_output.put_line(lpad('TABLE.COLUMN', fcw)
                                 || lpad('COUNT', cw)
                                 || lpad('MEAN', cw)
                                 || lpad('STD', cw)
                                 || lpad('MIN', cw)
                                 || lpad('P25', cw)
                                 || lpad('P50', cw)
                                 || lpad('P75', cw)
                                 || lpad('MAX', cw));

            OPEN c_attr;
            FETCH c_attr INTO current_attr;
            IF c_attr%notfound THEN
                CLOSE c_attr;
                RAISE no_number_attr;
            ELSE
                CLOSE c_attr;
                OPEN c_attr;
                LOOP
                    FETCH c_attr INTO current_attr;
                    EXIT WHEN c_attr%notfound;
                    OPEN c_data FOR 'select count('
                                    || current_attr.column_name
                                    || '),
                        to_char(avg('
                                    || current_attr.column_name
                                    || '), ''9.90EEEE''), 
                        to_char(stddev('
                                    || current_attr.column_name
                                    || '), ''9.90EEEE''), 
                        to_char(min('
                                    || current_attr.column_name
                                    || '), ''9.90EEEE''),
                        to_char(PERCENTILE_DISC(0.25) within group (order by '
                                    || current_attr.column_name
                                    || '), ''9.90EEEE''),
                        to_char(PERCENTILE_DISC(0.5) within group (order by '
                                    || current_attr.column_name
                                    || '), ''9.90EEEE''),
                        to_char(PERCENTILE_DISC(0.75) within group (order by '
                                    || current_attr.column_name
                                    || '), ''9.90EEEE''),
                        to_char(max('
                                    || current_attr.column_name
                                    || '), ''9.90EEEE'') 
                        from '
                                    || usr
                                    || '.'
                                    || tbl;

                    FETCH c_data INTO current_data;
                    dbms_output.put_line(lpad(lower(tbl
                                                    || '.'
                                                    || current_attr.column_name), fcw)
                                         || lpad(current_data.d_count, cw)
                                         || lpad(current_data.d_mean, cw)
                                         || lpad(current_data.d_std, cw)
                                         || lpad(current_data.d_min, cw)
                                         || lpad(current_data.d_q1, cw)
                                         || lpad(current_data.d_q2, cw)
                                         || lpad(current_data.d_q3, cw)
                                         || lpad(current_data.d_max, cw));

                END LOOP;

                CLOSE c_data;
                CLOSE c_attr;
            END IF;

        END IF;

    END IF;

EXCEPTION
    WHEN no_existing_schema THEN
        dbms_output.put_line('Schema does not exist.');
    WHEN no_existing_table THEN
        dbms_output.put_line('Table does not exist.');
    WHEN no_number_attr THEN
        dbms_output.put_line('Table with no number attributes.');
END;
/

EXECUTE mb20_nagdaz_fz1au9('mb18___db', 'historyitems');